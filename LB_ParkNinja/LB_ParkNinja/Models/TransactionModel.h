//
//  TransactionModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionModel : NSObject


@property (assign, nonatomic) NSInteger transactionId;

@property (strong, nonatomic) NSString *parkingName;
@property (strong, nonatomic) NSString *customerName;
@property (strong, nonatomic) NSString *plateNum;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *reservationDate;

@property (assign, nonatomic) NSInteger rate;
@property (strong, nonatomic) NSString  *succeedingRate;
@property (assign, nonatomic) NSInteger convenienceFee;
@property (assign, nonatomic) NSInteger discountPromo;
@property (strong, nonatomic) NSString *promoCode;

@property (assign, nonatomic) NSInteger totalPrice;
@property (assign, nonatomic) NSInteger subTotalPrice;
@property (strong, nonatomic) NSString *transactionCode;
@property (assign, nonatomic) NSInteger remainingTime;

@property (assign, nonatomic) BOOL isCancellable;
@property (assign, nonatomic) BOOL isCancelled;
@property (assign, nonatomic) BOOL isActive;

@property (assign, nonatomic) float longitude;
@property (assign, nonatomic) float latitude;



@property (strong, nonatomic) NSString *checkInTime;
@property (strong, nonatomic) NSString *checkOutTime;
@property (strong, nonatomic) NSString  *convenienceSucceedingRate;
@property (strong, nonatomic) NSString *tellerName;
@property (strong, nonatomic) NSString *eventName;
@property (assign, nonatomic) NSInteger subTotal;

@property (assign, nonatomic) NSInteger succeedingTotal;



- (id)initWithTransactionDictionary:(NSDictionary *)dictionary;
- (id)initWithTicketListDictionary:(NSDictionary *)dictionary;
- (id)initWithPushNotifDictionary:(id)dictionary;

@end
