//
//  SettingsVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 04/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ProfileVC.h"
#import "HttpManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "AppDelegate.h"
#import "ProfileImageCell.h"
#import "AccountInfoCell.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

#import "User.h"
#import "UserModel.h"

typedef NS_ENUM (NSInteger, CELL_INDEX)
{
    CELL_IMAGE,
    CELL_NAME,
    CELL_BDAY,
    CELL_MOBILE,
    CELL_EMAIL,
    CELL_PASSWORD
};

@interface ProfileVC ()
{
    UserModel *userModel;
    User *user;
    BOOL updateEmail;
    NSString *newEmail;
}

@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property (weak, nonatomic) IBOutlet UIView *bdayContainerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end

@implementation ProfileVC
{
    UIImage *profileImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    [userModel initWithUserProfileImageData:user];
    
    profileImage = [[UIImage alloc] initWithData:userModel.displayImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Profile";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
}

- (NSString *)setAccountValueWithRow:(NSInteger)row
{
    NSString *rowValue = @"";
    
    switch (row)
    {
        case CELL_NAME:
        {
            rowValue = [NSString stringWithFormat:@"%@", userModel.fullName];
            break;
        }
        
    case CELL_BDAY:
        {
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:([userModel.birthday longLongValue] / 1000)];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
            [dateFormat setDateFormat:@"MM/dd/yyyy"];
            NSString *dateString = [dateFormat stringFromDate:date];
            
            [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
            rowValue = dateString;
            break;
        }
        
    case CELL_MOBILE:
        {
            rowValue = userModel.mobileNo;
            break;
        }
        
    case CELL_EMAIL:
        {
            rowValue = userModel.email;
            break;
        }
        
    case CELL_PASSWORD:
        {
            rowValue = @"*************";
            break;
        }
    }
    
    return rowValue;
}

- (void)showBdayView
{
    [self.view endEditing:YES];
    
    self.bdayContainerView.hidden = NO;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([userModel.birthday longLongValue] / 1000)];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:date];
    NSString *stringDate = [NSString stringWithFormat:@"%@", dateString];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat1 setDateFormat:@"MM/dd/yyyy"];
    NSDate *selectedDate = [dateFormat1 dateFromString:stringDate];
    
    NSCalendar *calendar = [[NSCalendar alloc]
                            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setHour:8];
    
    NSDate *dateInput = [calendar dateByAddingComponents:components toDate:selectedDate options:0];
    self.datePicker.date = dateInput;
}

- (void)openGallery
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (NSString *)setAccountDetailsWithRow:(NSInteger)row
{
    NSString *rowTitle = @"";
    
    switch (row)
    {
        case CELL_NAME:
        {
            rowTitle = @"Name";
            break;
        }

        case CELL_BDAY:
        {
            rowTitle = @"Birthday";
            break;
        }
            
        case CELL_MOBILE:
        {
            rowTitle = @"Mobile No.";
            break;
        }
            
        case CELL_EMAIL:
        {
            rowTitle = @"Email Address";
            break;
        }
        
        case CELL_PASSWORD:
        {
            rowTitle = @"Password";
            break;
        }
    }
    
    return rowTitle;
}

- (void)updateUserDetails:(id)sender
{
    switch ([sender tag])
    {
        case CELL_IMAGE:
        {
            [self openGallery];
            break;
        }
            
        case CELL_NAME:
        {
            [self showUpdateNamePopUp];
            break;
        }
            
        case CELL_BDAY:
        {
            [self showBdayView];
            break;
        }
            
        case CELL_MOBILE:
        {
            [self showUpdateMobilePopUp];
            break;
        }
            
        case CELL_EMAIL:
        {
            [self showUpdateEmailPopUp];
            break;
        }
            
        case CELL_PASSWORD:
        {
            [self showUpdatePasswordPopUp];
            break;
        }
    }
}

- (void)showActivationPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Activation Code"
                                                                   message:@"Please enter the activation code we sent to your email."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *code = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     [self requestActivateAccountWithCode:code];
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Activation Code";
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - UIAlertController
- (void)showUpdateNamePopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Profile"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *full = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     if ([GlobalMethods checkInputForWhiteSpaces:full])
                                                     {
                                                         userModel.fullName = full;
                                                         
                                                         [self requestUpdateProfileDetails];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_FIELDS_INCOMPLETE];
                                                         [self presentViewController:alert animated:YES completion:nil];
                                                     
                                                     }
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"Full Name";
         textField.text = userModel.fullName;
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showUpdatePasswordPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Password"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *old = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     NSString *new = ((UITextField *)[alert.textFields objectAtIndex:1]).text;
                                                     NSString *confirm = ((UITextField *)[alert.textFields objectAtIndex:2]).text;
                                                     
                                                     
                                                     if (([GlobalMethods checkInputForWhiteSpaces:old] && [GlobalMethods checkInputForWhiteSpaces:new] && [GlobalMethods checkInputForWhiteSpaces:confirm]) && ([new isEqualToString:confirm]))
                                                     {
                                                         [self requestUpdatePasswordWithOld:old andNew:new];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_PASSWORD_INVALID];
                                                         [self presentViewController:alert animated:YES completion:nil];

                                                     }
                                                     
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"Old Password";
         textField.secureTextEntry = YES;
     }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"New Password";
         textField.secureTextEntry = YES;
     }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"Confirm New Password";
          textField.secureTextEntry = YES;
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showUpdateEmailPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Email"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *email = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     if ([GlobalMethods checkInputForWhiteSpaces:email] && [GlobalMethods NSStringIsValidEmail:email])
                                                     {
                                                         
                                                         updateEmail = YES;
                                                         [self requestUpdateEmailWithOld:userModel.email andNew:email];
                                                         newEmail = userModel.email;
                                                         userModel.email = email;
                                                     }
                                                     
                                                     else
                                                     {
                                                         updateEmail = NO;
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_EMAIL_INVALID];
                                                         [self presentViewController:alert animated:YES completion:nil];

                                                     }
                                         
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       updateEmail = NO;
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"New Email";
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showUpdateMobilePopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Mobile Number"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *mobile = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     if ([mobile length] == 11)
                                                     {
                                                         userModel.mobileNo = mobile;
                                                         
                                                         [self requestUpdateProfileDetails];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_MOBILE_INVALID];
                                                         [self presentViewController:alert animated:YES completion:nil];
                                                     }
                                                     
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"New Mobile Number";
         textField.keyboardType = UIKeyboardTypeNumberPad;
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - API Request
- (void)requestUpdateProfileDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    long birthday= [userModel.birthday longLongValue];
    
    if (userModel.nameSuffix == nil)
        userModel.nameSuffix = @"";
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", userModel.fullName, @"fullName", userModel.mobileNo, @"mobileNo", [NSNumber numberWithLong:birthday], @"birthday", nil];

    [[HttpManagerBlocks sharedInstance] postToUrl:URL_UPDATE_PROFILE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [GlobalMethods saveUserWithInfo:userInfo];
        
        [self.profileTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
//        if (updateEmail == NO) {
            UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_UPDATE_SUCCESS];
            [self presentViewController:alert animated:YES completion:nil];
//        } else {
        
//        }
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        user = [GlobalMethods getUser];
        userModel = [[UserModel alloc] initWithUserModel:user];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestUpdatePasswordWithOld:(NSString *)oldPass andNew:(NSString *)newPass
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", oldPass, @"password", newPass, @"newPassword", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_UPDATE_PASSWORD withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [self.profileTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_UPDATE message:MESSAGE_UPDATE_SUCCESS];
        [self presentViewController:alert animated:YES completion:nil];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        user = [GlobalMethods getUser];
        userModel = [[UserModel alloc] initWithUserModel:user];
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestUpdateEmailWithOld:(NSString *)oldEmail andNew:(NSString *)newEmail1
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:oldEmail, @"email", newEmail1, @"newEmail", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_UPDATE_EMAIL withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [GlobalMethods hideProgressHUD];
        
        [self showActivationPopUp];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        user = [GlobalMethods getUser];
        userModel = [[UserModel alloc] initWithUserModel:user];
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestActivateAccountWithCode:(NSString *)code
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *activateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:newEmail, @"email", code, @"activationKey", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_ACTIVATE_EMAIL withParameters:activateDictionary withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        UserModel *t = [[UserModel alloc] initWithUserInfoDictionary:responseDictionary];
        NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:t.email, @"email", t.fullName, @"fullName", nil];
        
        [GlobalMethods saveUserWithInfo:userInfo];
        
        user = [GlobalMethods getUser];
        userModel = nil;
        userModel = [[UserModel alloc] initWithUserModel:user];
        [userModel initWithUserProfileImageData:user];
        [self.profileTableView reloadData];
        
        updateEmail = NO;
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:serverError])
            errorStr = customDescription;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:errorStr
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       
                                                       [self showActivationPopUp];
                                                   }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (IBAction)dismissPickerView:(id)sender
{
    NSDate *today = [NSDate date]; // it will give you current date
    NSComparisonResult result;
    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
    
    result = [today compare:self.datePicker.date]; // comparing two dates
    
    if (result == NSOrderedAscending)
    {
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:@"Invalid Date" message:@"Kindly check your input date."];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *dateString = [dateFormat stringFromDate:self.datePicker.date];
        NSString *stringDate = [NSString stringWithFormat:@"%@", dateString];
        
        NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
        [dateFormat1 setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat1 setDateFormat:@"MM/dd/yyyy"];
        NSDate *selectedDate = [dateFormat1 dateFromString:stringDate];
        
        NSCalendar *calendar = [[NSCalendar alloc]
                                initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setHour:8];
        
        NSDate *dateInput = [calendar dateByAddingComponents:components toDate:selectedDate options:0];
        
        long timestamp = ([dateInput timeIntervalSince1970] * 1000) + 14400000;
        
        userModel.birthday = [NSString stringWithFormat:@"%ld", (long)timestamp];
        
        [self requestUpdateProfileDetails];
        
        self.bdayContainerView.hidden = YES;
    }
}

- (IBAction)cancelPickerView:(id)sender
{
    self.bdayContainerView.hidden = YES;
}


#pragma mark - UITableViewCell
- (ProfileImageCell *)setProfileImageCellWithRow:(NSInteger)row
{
    ProfileImageCell *cell = [self.profileTableView dequeueReusableCellWithIdentifier:@"accountinfo"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ProfileImageCell" owner:self options:nil];
        cell = (ProfileImageCell *)nibArray[0];
    }
    
    cell.profileImageView.image = profileImage;
    cell.editBtn.tag = row;
    [cell.editBtn addTarget:self action:@selector(updateUserDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([user.isGuest boolValue])
    {
        cell.profileImageView.image = [UIImage imageNamed:@"img_default_profile"];
    }
    
    else
    {
        if ([user.displayImage length] > 0)
            cell.profileImageView.image = [UIImage imageWithData:user.displayImage];
        else
            cell.profileImageView.image = [UIImage imageNamed:@"img_default_profile"];
        
        
    }
    
    cell.profileImageView.layer.cornerRadius = 50.0f;
    cell.profileImageView.layer.masksToBounds = YES;

    
    return cell;
}

- (AccountInfoCell *)setAccountInfoCellWithRow:(NSInteger)row
{
    AccountInfoCell *cell = [self.profileTableView dequeueReusableCellWithIdentifier:@"details"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"AccountInfoCell" owner:self options:nil];
        cell = (AccountInfoCell *)nibArray[0];
    }
    
    cell.infoLabel.text = [self setAccountDetailsWithRow:row];
    cell.valueLabel.text = [self setAccountValueWithRow:row];
    
    cell.editBtn.tag = row;
    [cell.editBtn addTarget:self action:@selector(updateUserDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return [self setProfileImageCellWithRow:indexPath.row];
    
    return [self setAccountInfoCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 175.0f;
    
    return 70.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.profileTableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    profileImage = [self imageWithImage:image scaledToSize:CGSizeMake(100, 100)];
    profileImage = image;
    
    [GlobalMethods saveUserWithImage:profileImage];
    
    [self.profileTableView reloadData];
}

- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
