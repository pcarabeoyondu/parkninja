//
//  ParkingLotDetailsCell.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 01/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingLotDetailsCell.h"

@implementation ParkingLotDetailsCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.hourlyRateLabel.frame.size.height < 1) { //fix for overlapping labels when there's no hourly rate for bookable.
        self.topSpaceBetweenHourlyRateAndSucceedingRate.constant += 15;
    }
}
@end
