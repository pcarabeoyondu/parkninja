//
//  GlobalMethods.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/13/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "GlobalMethods.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

#import <Google/CloudMessaging.h>

@implementation GlobalMethods

+ (NSManagedObjectContext *)getContext
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate managedObjectContext];
}

+ (NSArray *)fetchAllWithEntityName:(NSString *)entityName
{
    NSManagedObjectContext *context = [self getContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

+ (User *)getUser
{
    User *user;
    
    NSArray *users = [self fetchAllWithEntityName:@"User"];
    
    if ([users count] > 0)
        user = [users objectAtIndex:0];
    
    return user;
}

+ (void)saveUserWithInfo:(NSDictionary *)userInfo
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    //filter userInfoData
    NSMutableDictionary *filteredUserInfo = [userInfo mutableCopy];
    for (NSString *key in userInfo.allKeys) {
        id data = [userInfo objectForKey:key];
        if ([data isEqual:[NSNull null]]) {
            data = @"";
            [filteredUserInfo setObject:data forKey:key];
        }
    }
    userInfo = [filteredUserInfo mutableCopy];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.email = userInfo[@"email"];
    user.fullName = userInfo[@"fullName"];
    user.userId = userInfo[@"id"];
    user.userStatus = userInfo[@"userStatus"];
    user.userName = userInfo[@"username"];
    
    //there are times the birthday returned is <null> so we handled that on top by changing it to "" of [NSString class] type.
    user.birthday = ([[userInfo objectForKey:@"birthday"] isKindOfClass:[NSNumber class]]) ? userInfo[@"birthday"] : [NSNumber numberWithInteger:0];
    user.mobileNo = userInfo[@"mobileNo"];
//    user.facebookId = userInfo[@"facebookId"];
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)saveUserWithImage:(UIImage *)image
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.displayImage =  UIImagePNGRepresentation(image);
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)saveUserType:(BOOL)isGuest
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.isGuest = [NSNumber numberWithBool:isGuest];
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)savePaymentStatus:(BOOL)willProceed
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.willProceedToPayment = [NSNumber numberWithBool:willProceed];
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)didReceivePushNotif:(BOOL)received
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.didReceivePushNotif = [NSNumber numberWithBool:received];
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)willNavigateToParking:(BOOL)navigate lat:(float)latitude long:(float)longitude
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.willNavigateToParking = [NSNumber numberWithBool:navigate];
    user.parkingLat = [NSNumber numberWithFloat:latitude];
    user.parkingLong = [NSNumber numberWithFloat:longitude];
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
        NSLog(@"NO INTERNET CONNECTION");
    
    return networkStatus != NotReachable;
}

+ (void)logout
{    
    [[[SDWebImageManager sharedManager] imageCache] clearDisk];
    [[[SDWebImageManager sharedManager] imageCache] clearMemory];
    
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    [context deleteObject: user];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"failed to save");
    }
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setRootViewControllerWithSB:@"LoginSB"];
    
    
}

+ (UIAlertController *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    return alert;

}

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)checkInputForWhiteSpaces:(NSString *)inputString
{
    
    if ([inputString isKindOfClass:[NSNull class]] == YES)
        return NO;
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [inputString stringByTrimmingCharactersInSet:charSet];
    
    if ([trimmedString isEqualToString:@""] || (trimmedString == (id)[NSNull null]) || ([trimmedString length] == 0))
    {
        return NO;
    }
    
    return YES;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

//MBProgressHUD
+ (MBProgressHUD *)showProgressHUD
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.mode = MBProgressHUDModeCustomView;
    
    HUD.color = [UIColor clearColor];
    
    UIImageView *activityImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading_000"]];

    NSString *imageName;
    NSMutableArray *loadingImageArray = [NSMutableArray array];
    
    for (int i = 1 ; i < 75; i ++) {
        imageName = [NSString stringWithFormat:@"loading_%03d",i];
        [loadingImageArray addObject:[UIImage imageNamed:imageName]];        
    }
    
    activityImageView.animationImages = loadingImageArray;
    activityImageView.animationDuration = 1.0;
    
    [activityImageView startAnimating];
    
    HUD.customView = activityImageView;
//    HUD.labelText = @"Loading...";
 
    return HUD;
}

+ (void)hideProgressHUD
{
    [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
}

@end
