//
//  MapParkingMarker.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/7/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "ParkingLotModel.h"

@interface MapParkingMarker : GMSMarker

@property (strong, nonatomic) ParkingLotModel *parkingLot;

@end
