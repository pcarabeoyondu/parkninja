//
//  TermsView.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 4/8/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsView : UIView
@property (weak, nonatomic) IBOutlet UILabel *termsDescription;

@end
