//
//  LoginFBUserModel.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 10/11/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (id)initWithUserInfoDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.userId = [dictionary objectForKey:@"id"];
        self.activationKey = [dictionary objectForKey:@"activationKey"];
        self.birthday = [dictionary objectForKey:@"birthday"];
        self.email = [dictionary objectForKey:@"email"];
        self.facebookId = [dictionary objectForKey:@"facebookId"];
        
        self.fullName = [dictionary objectForKey:@"fullName"];
        self.emailNew = [dictionary objectForKey:@"newEmail"];
        
        self.password = [dictionary objectForKey:@"password"];
        self.passwordNew = [dictionary objectForKey:@"newPassword"];
        self.roleId = [dictionary objectForKey:@"roleId"];
        self.roles = [dictionary objectForKey:@"roles"];
        self.status = [dictionary objectForKey:@"status"];
        self.userStatus = [dictionary objectForKey:@"userStatus"];
        self.userName = [dictionary objectForKey:@"username"];
        self.mobileNo = [dictionary objectForKey:@"mobileNo"];
        
    }
    
    return self;
}

- (id)initWithUserModel:(User *)user
{
    self = [super init];
    
    if (self)
    {
        self.userId = user.userId;
        self.birthday = user.birthday;
        self.email = user.email;
        self.fullName = user.fullName;
        self.mobileNo = user.mobileNo;
        self.userName = user.userName;
        self.userStatus = user.userStatus;
    }
    
    return self;
}

- (void)initWithUserProfileImageData:(User *)user
{
    self.displayImage = user.displayImage;
}
    
@end
