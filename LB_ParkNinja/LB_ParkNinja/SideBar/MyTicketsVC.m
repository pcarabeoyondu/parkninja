//
//  MyTicketsVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 04/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "MyTicketsVC.h"
#import "TicketCell.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "UserModel.h"
#import "User.h"
#import "TransactionModel.h"
#import "ETicketVC.h"
#import "ParkingReserveEstimationModel.h"
#import "ParkingLotModel.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface MyTicketsVC () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *ticketListTableView;

@property (strong, nonatomic) NSMutableArray *ticketListArray;
@property (strong, nonatomic) NSMutableArray *parkingListArray;

@end

@implementation MyTicketsVC
{
    User *user;
    UserModel *userModel;
    
    TransactionModel *transModel;
    ParkingLotModel *parkingModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    
    [self setupNavigationItem];
    
    [self initializeView];
    
    self.parkingListArray = [NSMutableArray array];
    
    //we only add refresh control once
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.ticketListTableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self requestTransactionDetails];
    
    [refreshControl endRefreshing];
}

#pragma mark - Private Methods
- (void)initializeView
{
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    if ([GlobalMethods isConnected])
        [self requestTransactionDetails];
}

- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Ticket List";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
}

- (IBAction)saveAndCloseView:(id)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
}

#pragma mark - API REQUEST
- (void)requestTransactionDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_TRANSACTIONS withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        self.ticketListArray = nil;
        self.ticketListArray = [NSMutableArray array];
        self.parkingListArray = nil;
        self.parkingListArray = [NSMutableArray array];
        
        for (NSDictionary *data in [responseDictionary objectForKey:@"results"])
        {
            TransactionModel *transactModel = [[TransactionModel alloc] initWithTicketListDictionary:data];
            ParkingLotModel *parkingLotModel = [[ParkingLotModel alloc] initWithTransactionDictionary:data];
            
            [self.ticketListArray addObject:transactModel];
            [self.parkingListArray addObject:parkingLotModel];
            
        }
        
        NSArray *t = [[self.ticketListArray reverseObjectEnumerator] allObjects];
        [self.ticketListArray removeAllObjects];
        [self.ticketListArray addObjectsFromArray:t];
        
        t = [[self.parkingListArray reverseObjectEnumerator] allObjects];
        [self.parkingListArray removeAllObjects];
        [self.parkingListArray addObjectsFromArray:t];
        
        [self.ticketListTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - UITableViewCell
- (TicketCell *)setTicketCellWithRow:(NSInteger)row
{
    TicketCell *cell = [self.ticketListTableView dequeueReusableCellWithIdentifier:@"TicketCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"TicketCell" owner:self options:nil];
        cell = (TicketCell *)nibArray[0];
    }
    
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:row];
    cell.referenceLabel.text = transactModel.transactionCode;
    
    if (transactModel.remainingTime <= 0 || transactModel.isCancelled == YES || transactModel.isActive == NO) {
        cell.referenceLabel.textColor = [GlobalMethods colorFromHexString:@"#FF0000"];
    } else {
        cell.referenceLabel.textColor = [GlobalMethods colorFromHexString:@"#00FF00"];
    }
    cell.parkingNameLabel.text = [NSString stringWithFormat:@"Reserved At: %@", transactModel.parkingName];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [GlobalMethods colorFromHexString:@"#F1F7F7"];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.ticketListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setTicketCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    transModel = [self.ticketListArray objectAtIndex:indexPath.row];
    parkingModel = [self.parkingListArray objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:SEGUE_SHOW_ETICKET_FROM_LIST sender:self];
    
    [self.ticketListTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:indexPath.row];
    
    if (transactModel.remainingTime <= 0 || transactModel.isCancelled == YES || transactModel.isActive == NO) {
        return @[[UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            
            
            [[HttpManagerBlocks sharedInstance] postToUrl:URL_DELETE_TICKET withParameters:@{@"id" : [NSString stringWithFormat:@"%ld", transactModel.transactionId]} withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {

                [self.ticketListArray removeObjectAtIndex:indexPath.row];
                [self.parkingListArray removeObjectAtIndex:indexPath.row];
                
                [self.ticketListTableView reloadData];
                
                [GlobalMethods hideProgressHUD];
                
            } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
                
                NSString *errorStr = serverError;
                
                if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
                    errorStr = customDescription;
                
                UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
                [self presentViewController:alert animated:YES completion:nil];
                
                [GlobalMethods hideProgressHUD];
            }];
            
            
        }]];
    }
    
    return nil;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:indexPath.row];
    
    if (transactModel.remainingTime <= 0 || transactModel.isCancelled == YES || transactModel.isActive == NO) {
        return YES;
    }
    
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:indexPath.row];
    
    if (transactModel.remainingTime <= 0 || transactModel.isCancelled == YES || transactModel.isActive == NO) {
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:indexPath.row];
    
    if (transactModel.remainingTime <= 0 || transactModel.isCancelled == YES || transactModel.isActive == NO) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            [[HttpManagerBlocks sharedInstance] postToUrl:URL_DELETE_TICKET withParameters:@{@"id" : [NSString stringWithFormat:@"%ld", transactModel.transactionId]} withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
                
                [self.ticketListArray removeObjectAtIndex:indexPath.row];
                [self.parkingListArray removeObjectAtIndex:indexPath.row];
                
                [self.ticketListTableView reloadData];
                
                [GlobalMethods hideProgressHUD];
                
            } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
                
                NSString *errorStr = serverError;
                
                if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
                    errorStr = customDescription;
                
                UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
                [self presentViewController:alert animated:YES completion:nil];
                
                [GlobalMethods hideProgressHUD];
            }];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ETicketVC *vc = (ETicketVC *)segue.destinationViewController;
    vc.transactionModel = transModel;
    vc.parkingModel = parkingModel;
    vc.isNavigationBarHidden = FALSE;
}

@end
