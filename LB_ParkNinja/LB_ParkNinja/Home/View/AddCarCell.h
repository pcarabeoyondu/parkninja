//
//  AddCar.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 22/03/2016.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCarCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *makeTextField;
@property (strong, nonatomic) IBOutlet UITextField *plateNumTextField;

@end
