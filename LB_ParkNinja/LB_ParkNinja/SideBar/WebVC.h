//
//  WebVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 3/7/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebVC : UIViewController

@property (strong, nonatomic) NSString *webPath;

@end
