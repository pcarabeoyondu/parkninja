//
//  ParkingSummaryVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 03/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingSummaryVC.h"
#import "ParkingSummaryCell.h"

#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "UserModel.h"
#import "CarModel.h"
#import "PaymentDetailsVC.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ETicketVC.h"
#import "Flurry.h"
#import "TransactionModel.h"
#import "ParkingLotModel.h"

@interface ParkingSummaryVC ()

@property (strong, nonatomic) IBOutlet UITableView *parkingSummaryTableView;
@property (strong, nonatomic) NSMutableArray *promoListArray;
@property (strong, nonatomic) PromoCodeModel *promoModel;
@property (strong, nonatomic) ParkingReserveEstimationModel *estimateModel;
@property (weak, nonatomic) IBOutlet UIImageView *bgPromoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *promocodeStatus;

@end

@implementation ParkingSummaryVC
{
    UserModel *userModel;
    User *user;
    CarModel *car;
    
    NSString *promoCodeInput;
    NSInteger totalPrice;
    BOOL hasValidPromoCode;
    
    ParkingLotModel *parkingTransactModel;
    TransactionModel *transactModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    self.promocodeStatus.text = @"";
    [self requestPromoList];
    [self requestReserveEstimation];
    
    _indicator.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Reservation";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (PromoCodeModel *)findPromoCodeWithName:(NSString *)input
{
    PromoCodeModel *foundModel = NULL;
    for (PromoCodeModel *model in self.promoListArray)
    {
        if (model.code && [model.code caseInsensitiveCompare:input]==NSOrderedSame)
        {
            foundModel = model;
        }
    }
    return foundModel;
}

- (BOOL)validateInputPromoCode:(NSString *)input
{
    PromoCodeModel *foundModel = [self findPromoCodeWithName:input];
    NSString *errorStr = @"";
    if (foundModel) {
        if (foundModel.status && [foundModel.status caseInsensitiveCompare:@"EXPIRED"] == NSOrderedSame) {
            foundModel = nil;
            errorStr = @"Invalid or expired promo code.";
            
        }else if (foundModel.available < 1) {
            foundModel = nil;
            errorStr = @"This promo code has reached its number of use.";
        }
    }else {
        errorStr = ![input isEqualToString:@""] ? @"Invalid or expired promo code." : @"";
    }
    
    self.promocodeStatus.text = errorStr;
    self.promoModel = foundModel;   //nil or not
    
    return (foundModel != NULL);
}

- (void)updatePaymentDetailsWithDiscount:(BOOL)discount
{
    NSInteger price = self.estimateModel.ratePrice + self.estimateModel.convenienceFee;
    
    if (discount)
    {
        if (self.promoModel.isDiscountRateCurrency)
        {
            totalPrice = price - self.promoModel.discountRateCurrency;
        }
        else
        {
            totalPrice = price - (price * (self.promoModel.discountRatePercentage / 100));
        }
        
        if (totalPrice < 0 )
            totalPrice = 0;
        
        [self.bgPromoImageView setImage:[UIImage imageNamed:@"bg_text_box_selected"]];
        self.indicator.hidden = NO;
    }
    else
    {
        totalPrice = price;
        
        [self.bgPromoImageView setImage:[UIImage imageNamed:@"bg_text_box_error"]];
        self.indicator.hidden = YES;
    }
    
    [self.parkingSummaryTableView reloadData];
}

#pragma mark - IBAction
- (IBAction)showPaymentDetails:(id)sender
{
    [self.view endEditing:YES];
    
    if (!hasValidPromoCode && [promoCodeInput length] > 0) {
        [self validateInputPromoCode:promoCodeInput];
        return;
    }
    
    [self performSegueWithIdentifier:SEGUE_SHOW_PAYMENT_DETAILS sender:self];
}

#pragma mark - API Request
- (void)requestPromoList
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    self.promoListArray = nil;
    self.promoListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", userModel.email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PROMO_CODE_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            PromoCodeModel *codeModel = [[PromoCodeModel alloc] initPromoCodeWithDictionary:(NSDictionary *)array];
            NSLog(@"%@", array);
            [self.promoListArray addObject:codeModel];
        }
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestReserveEstimation
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    self.estimateModel.earlyBird = FALSE;
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: userModel.email, @"email", [NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", nil];
    
    if (self.estimateModel.promoCode.length !=0)
        [userInfo setObject:self.estimateModel.promoCode forKey:@"promoCode"];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_RESERVE_ESTIMATION withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        self.estimateModel = [[ParkingReserveEstimationModel alloc] initParkingEstimationWithDictionary:responseDictionary];
        
        [self updatePaymentDetailsWithDiscount:NO];
        
        [self.parkingSummaryTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
        
    }];
}

- (void)requestProceedPurchase:(void(^)(BOOL finished))completion
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSMutableDictionary *userInfo =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email",[NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", [NSNumber numberWithInteger:self.promoModel.promoCodeId], @"promoCodeId", [NSNumber numberWithInteger:self.carModel.carId], @"carId", nil];
    
    __block NSMutableDictionary *flurryParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.parkingLotModel.location, @"parkingLocation", [NSNumber numberWithInteger:self.carModel.carId], @"carId", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PROCEED_PURCHASE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode)
     {
         
         transactModel = [[TransactionModel alloc] initWithTransactionDictionary:responseDictionary];
         parkingTransactModel = [[ParkingLotModel alloc] initWithTransactionDictionary:responseDictionary];
         
         [flurryParameters setObject:@"Success" forKey:@"transactionStatus"];
         [Flurry logEvent:@"Parking_Reserve" withParameters:flurryParameters];
         
         completion(YES);
         
         [GlobalMethods hideProgressHUD];
         
     } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
         
         [flurryParameters setObject:@"Failed" forKey:@"transactionStatus"];
         if (![serverError isKindOfClass:[NSNull class]]) {
            [flurryParameters setObject:@"" forKey:@"transactionMessage"];
         } else {
             [flurryParameters setObject:serverError forKey:@"transactionMessage"];
         }
         
         [Flurry logEvent:@"Parking_Reserve" withParameters:flurryParameters];
         
         NSString *errorStr = serverError;
         
         if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
             errorStr = customDescription;
         
         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
         [self presentViewController:alert animated:YES completion:nil];
         
         completion(NO);
         
         [GlobalMethods hideProgressHUD];
     }];
}


#pragma mark - ParkingInfoCell
- (ParkingSummaryCell *)setParkingSummaryWithRow:(NSInteger)row
{
    ParkingSummaryCell *cell = [self.parkingSummaryTableView dequeueReusableCellWithIdentifier:@"ParkingSummaryCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingSummaryCell" owner:self options:nil];
        cell = (ParkingSummaryCell *)nibArray[0];
    }
    
    cell.parkingNameLabel.text = self.parkingLotModel.location;
    cell.addressLabel.text = self.parkingLotModel.address;
    cell.carMakeLabel.text = self.carModel.make;
    cell.plateNumLabel.text = self.carModel.plateNum;
    
    cell.rateLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)self.estimateModel.ratePrice];
    cell.convenienceFeeLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)self.estimateModel.convenienceFee];
    
    cell.totalPriceLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)totalPrice];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingSummaryWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 400.0f;
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SHOW_PAYMENT_DETAILS])
    {
        PaymentDetailsVC *vc = (PaymentDetailsVC *)segue.destinationViewController;
        vc.parkingLotModel = self.parkingLotModel;
        vc.estimateModel = self.estimateModel;
        vc.carModel = self.carModel;
        vc.promoModel = self.promoModel;
        
        return;
    }
    
    ETicketVC *vc = (ETicketVC *)segue.destinationViewController;
    vc.transactionModel = transactModel;
    vc.parkingModel = parkingTransactModel;
    vc.isNavigationBarHidden = YES;
}

#pragma mark - UITextFieldDelegate
- (void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setViewMovedUp:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_parkingSummaryTableView scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    promoCodeInput = textField.text;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    if ([self validateInputPromoCode:newString])
    {
        promoCodeInput = newString;
        hasValidPromoCode = YES;
    }
    
    else
    {
        self.promoModel = nil;
        hasValidPromoCode = NO;
    }
    
    [self updatePaymentDetailsWithDiscount:hasValidPromoCode];

    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    
    return NO; // We do not want UITextField to insert line-breaks.
}

@end
