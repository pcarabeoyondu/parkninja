//
//  SignUpDetailsCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserSignUpModel.h"


@protocol SignUpDetailCellsDelegate;

@interface SignUpDetailsCell : UITableViewCell

@property (assign, nonatomic) id <SignUpDetailCellsDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UILabel *termsAndConditionLabel;

@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *middleInitialTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameSuffixTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumTextField;

@property (strong, nonatomic) IBOutlet UIButton *birthdayButton;
@property (weak, nonatomic) IBOutlet UIButton *calendarButton;
@property (weak, nonatomic) IBOutlet UIButton *showPasswordButton;

- (void)initDetails;
- (void)setSignUpDetailsWithModel:(UserSignUpModel *)model;

@end

@protocol SignUpDetailCellsDelegate <NSObject>

- (void)showTermsAndConditionsPage;

@end
