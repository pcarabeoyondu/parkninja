//
//  ReceiptVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 4/2/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ReceiptVC.h"
#import "ReceiptCell.h"
#import "GlobalMethods.h"
#import "HomeVC.h"
#import "GlobalConstants.h"

@interface ReceiptVC ()

@property (weak, nonatomic) IBOutlet UITableView *receiptTableView;
@property (strong, nonnull) TransactionModel *transactionModel;

@end

@implementation ReceiptVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = TRUE;
    
    NSDictionary *pushDict = (NSDictionary *)[[AppDelegate appDelegate] pushNotifDictionary];
    self.transactionModel = [[TransactionModel alloc] initWithPushNotifDictionary:pushDict];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (IBAction)dismissVC:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ParkingInfoCell
- (ReceiptCell *)setReceiptCellWithRow:(NSInteger)row
{
    NSString *cellName = @"ReceiptCell";
    
    if ([self.transactionModel.eventName isEqualToString:PUSH_NO_SHOW])
        cellName = @"NoShowReceiptCell";
    
    ReceiptCell *cell = [self.receiptTableView dequeueReusableCellWithIdentifier:cellName];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
        cell = (ReceiptCell *)nibArray[0];
    }
    
    cell.parkedAtLabel.text = self.transactionModel.parkingName;
    cell.customerNameLabel.text = self.transactionModel.customerName;
    cell.plateNumLabel.text = self.transactionModel.plateNum;
    cell.reservationDateLabel.text = self.transactionModel.reservationDate;
    cell.checkedInTimeLabel.text = self.transactionModel.checkInTime;
    cell.checkedOutLabel.text = self.transactionModel.checkOutTime;
    cell.tellerNameLabel.text = self.transactionModel.tellerName;
    
    
    cell.flatRateLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long) self.transactionModel.rate];
    cell.succeedingRateTotalLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long) self.transactionModel.succeedingTotal];
    cell.succeedingRateLabel.text = self.transactionModel.succeedingRate;
    
    cell.convenienceFeeTotalLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long) self.transactionModel.convenienceFee];
    cell.convenienceFeeLabel.text = self.transactionModel.convenienceSucceedingRate;
    
    if ([GlobalMethods checkInputForWhiteSpaces:self.transactionModel.promoCode])
    {
        NSMutableAttributedString *promoCode = [[NSMutableAttributedString alloc] initWithString:@"Promo Code: "];
        [promoCode addAttribute:(NSString *)NSForegroundColorAttributeName
                         value:[GlobalMethods colorFromHexString:@"#6D6D6D"]
                         range:(NSRange){0,[promoCode length]}];
        
        NSMutableAttributedString *promoName = [[NSMutableAttributedString alloc] initWithString:self.transactionModel.promoCode];
        [promoName addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#0C9BAA"]
                      range:(NSRange){0,[promoName length]}];
        
        NSMutableAttributedString *comString = [[NSMutableAttributedString alloc] initWithAttributedString:promoCode];
        [comString appendAttributedString:promoName];

        cell.promoCodeLabel.attributedText = comString;
    }
    else
        cell.promoCodeLabel.text = @"";
    

    cell.discountTotalLabel.text = [NSString stringWithFormat:@"-(Php %ld.00)", (long) self.transactionModel.discountPromo];
    
    cell.subTotalLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long) self.transactionModel.subTotal];
    cell.totalPriceLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long) self.transactionModel.totalPrice];
    
    
    
    return cell;
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setReceiptCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.transactionModel.eventName isEqualToString:PUSH_NO_SHOW])
        return 630.0f;
    
    return 710.0f;
}


@end
