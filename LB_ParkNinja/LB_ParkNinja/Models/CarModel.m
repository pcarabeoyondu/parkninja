//
//  CarModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "CarModel.h"

@implementation CarModel

- (id)initWithCarDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.carId = [[dictionary objectForKey:@"id"] integerValue];
        self.make = [dictionary objectForKey:@"make"];
        self.model = [dictionary objectForKey:@"model"];
        self.color = [dictionary objectForKey:@"color"];
        self.plateNum = [dictionary objectForKey:@"plateNo"];
        
        self.status = [dictionary objectForKey:@"status"];
        self.email = [dictionary objectForKey:@"email"];
        self.user= [dictionary objectForKey:@"user"];
    }
    
    return self;
}

@end
