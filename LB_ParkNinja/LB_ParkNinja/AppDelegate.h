//
//  AppDelegate.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Google/CloudMessaging.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GGLInstanceIDDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSDictionary *pushNotifDictionary;

@property (strong, nonatomic) NSString *registrationToken;

- (void)saveContext;

- (void)setRootViewControllerWithSB:(NSString *)sb;

- (void)changeNavBarToClearColor;

- (void)changeNavBarToOriginal;

+ (AppDelegate *)appDelegate;


@end

