//
//  User.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface User : NSManagedObject

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *activationKey;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *facebookId;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *roleId;
@property (strong, nonatomic) NSString *roles;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *userStatus;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *mobileNo;

@property (strong, nonatomic) NSNumber *birthday;
@property (strong, nonatomic) NSNumber *isGuest;
@property (strong, nonatomic) NSNumber *willProceedToPayment;
@property (strong, nonatomic) NSNumber *didReceivePushNotif;
@property (strong, nonatomic) NSNumber *willNavigateToParking;
@property (strong, nonatomic) NSNumber *parkingLat;
@property (strong, nonatomic) NSNumber *parkingLong;

@property (strong, nonatomic) NSData *displayImage;
@property (strong, nonatomic) NSData *parkingLotModel;


@end
