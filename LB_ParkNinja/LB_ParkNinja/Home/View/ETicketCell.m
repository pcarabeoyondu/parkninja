//
//  ETicketCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/5/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ETicketCell.h"
#import "GlobalMethods.h"

@implementation ETicketCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)initDetailsWithTransactionModel:(TransactionModel *)model
{
    
    self.parkSiteLabel.text = model.parkingName;
    self.nameLabel.text = model.customerName;
    self.plateNumLabel.text = model.plateNum;
    self.dateReservationLabel.text = model.reservationDate;
    self.rateLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)model.rate];
    self.convenienceFeeLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)model.convenienceFee];
    self.promoCodeNameLabel.text = model.promoCode;
    self.discounTotalLabel.text = [NSString stringWithFormat:@"-(Php %ld.00)", (long)model.discountPromo];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)model.totalPrice];
    self.subTotalLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)model.subTotalPrice];
    self.referenceNumberLabel.text = model.transactionCode;
    self.succeedingHourLabel.text = model.succeedingRate;
    self.succeedingTotalLabel.text = [NSString stringWithFormat:@"Php %ld.00", (long)model.succeedingTotal];
    
    
    NSMutableAttributedString *discount = [[NSMutableAttributedString alloc] initWithString:@"Discount "];
    [discount addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#0C0C0C"]
                      range:(NSRange){0,[discount length]}];
    
    NSMutableAttributedString *promo = [[NSMutableAttributedString alloc] initWithString:@"PROMO"];
    [promo addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#FA9F1B"]
                      range:(NSRange){0,[promo length]}];
    
    NSMutableAttributedString *comString = [[NSMutableAttributedString alloc] initWithAttributedString:discount];
    [comString appendAttributedString:promo];
    
    self.discountPromoLabel.attributedText = comString;
}

@end
