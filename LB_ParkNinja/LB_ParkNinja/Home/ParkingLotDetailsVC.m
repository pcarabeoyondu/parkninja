//
//  ParkingLotDetailsVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/26/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingLotDetailsVC.h"
#import "ParkingLotDetailsCell.h"
#import "GlobalMethods.h"
#import "UserVehicleVC.h"

#import "GlobalConstants.h"
#import "HttpManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "HomeVC.h"

#import "LoginVC.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ParkingLotDetailsVC ()

@property (strong, nonatomic) IBOutlet UITableView *parkingLotDetailsTableView;
@property (strong, nonatomic) NSMutableArray *parkingList;

@property (weak, nonatomic) IBOutlet UIButton *reserveNowBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end

@implementation ParkingLotDetailsVC
{
    User *user;

    UIImage *parkingImage;
    BOOL isParkingImageLoaded;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //we update the constraint to make the table bigger if not bookable (based on mockup)
    if (!self.model.isBookable) {
        self.bottomConstraint.constant = -66;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];

//not requesting details anymore because the list is refreshed every 30 sec and complete info is returned when list is fetched instead, we just set up the details immediately.
//    [self requestParkingDetails];
    [self setUpDetails];
    
    [self setupNavigationBar];
    [self setupNavigationItem:self.navigationItem];
    
    [self.navigationController.navigationBar setHidden:NO];
    
    user = [GlobalMethods getUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestTransitionToPayment)
                                                 name:@"RequestTransitionToPayment"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods
- (void)setupNavigationItem:(UINavigationItem*)navigationItem
{
    [self setupRightNavigationItem:navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = _model.location;
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestTransitionToPayment
{
    [self performSegueWithIdentifier:SEGUE_SHOW_USER_INFORMATION sender:self];
}

- (void)checkIfReservable
{
    if (!_model.isBookable)
        [self.reserveNowBtn setHidden:TRUE];
    else if (_model.availableSlots == 0) {
        [self.reserveNowBtn setBackgroundImage:nil forState:UIControlStateDisabled];
        [self.reserveNowBtn setEnabled:FALSE];
    } else
        [self.reserveNowBtn setEnabled:TRUE];
}

#pragma mark - IBAction
- (IBAction)reserveNow:(id)sender
{
    [GlobalMethods savePaymentStatus:TRUE];
    
    if ([user.isGuest boolValue])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginSB" bundle:nil];
        UINavigationController *moreNavController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"guestNavController"];
        moreNavController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.navigationController.topViewController presentViewController:moreNavController animated:YES completion:nil];
        return;
    }
    
    [self performSegueWithIdentifier:SEGUE_SHOW_USER_INFORMATION sender:self];
}

- (IBAction)navigateParkingLocation:(id)sender
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.model forKey:@"ParkingModel"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RequestRoute" object:nil userInfo:userInfo];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UserVehicleVC *vc = (UserVehicleVC *)segue.destinationViewController;
    vc.parkingLotModel = self.model;
    vc.locationAddress = self.locationAddress;
}

#pragma mark - API Request
- (void)requestParkingDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInteger:_model.parkingId], @"parkingLotId", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PARKING_DETAILS withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        _model = [[ParkingLotModel alloc] initWithParkingLotDictionary:responseDictionary];
        
        [self setUpDetails];
        
        [GlobalMethods hideProgressHUD];

    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];

        [GlobalMethods hideProgressHUD];
    }];
}

- (void)setUpDetails
{
    [self requestDistanceWithLat:_model.latitude lon:_model.longitude];
    [self checkIfReservable];
}

- (void)requestDistanceWithLat:(float)latitude lon:(float)longitude
{
    if (![[HomeVC sharedInstance] isLocationServicesAuthorized]) {
        NSLog(@"not enabled");
        return;
    }
    
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSString *parkingCoordinate = [NSString stringWithFormat:@"%f,%f", latitude, longitude];
    
    NSString *baseUrl = [NSString stringWithFormat:URL_REQUEST_GOOGLE_ROUTE, self.location.coordinate.latitude, self.location.coordinate.longitude, parkingCoordinate];
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:baseUrl withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSDictionary *routes = nil;
        if (((NSArray *) responseDictionary[@"routes"]).count > 0) {
            routes = responseDictionary[@"routes"][0];
        }
        
        NSDictionary *legs = nil;
        if (((NSArray *) routes[@"legs"]).count > 0) {
            legs = routes[@"legs"][0];
        }
        
        NSDictionary *distance = legs[@"distance"];
        NSString *distanceText = distance[@"text"];
        NSLog(@"TEST %@", legs[@"distance"]);
        self.distance = distanceText;
        
        [self.parkingLotDetailsTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - ParkingInfoCell
- (ParkingLotDetailsCell *)setParkingDetailsWithRow:(NSInteger)row
{
    NSString *cellName = @"NonBookableParkingCell";
    
    if (self.model.isBookable)
        cellName = @"ParkingLotDetailsCell";
    
    
    ParkingLotDetailsCell *cell = [self.parkingLotDetailsTableView dequeueReusableCellWithIdentifier:cellName];
    

    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingLotDetailsCell" owner:self options:nil];
        cell = (ParkingLotDetailsCell *)nibArray[0];
    }
 
    cell.parkingNameLabel.text = self.model.location;
    cell.parkingAddressLabel.text = self.model.address;
    cell.availableSlotsLabel.text = [NSString stringWithFormat:@"%ld", (self.model.isBookable? (long)self.model.availableSlots : (long)self.model.totalSlots)];
    cell.operatingHoursLabel.text = self.model.operatingHours;
    cell.hourlyRateLabel.text = self.model.hourlyRate;
    cell.succeedingRateLabel.text = self.model.succeedingRate;
    cell.typeLabel.text = self.model.type;
    cell.distanceLabel.text = self.distance;
    
    //exclusive for non bookable
    cell.holidayWeekendRateLabel.text = self.model.weekendRate;
    cell.overnightRateLabel.text = self.model.overnightRate;
    cell.motorcycleRateLabel.text = self.model.motorcycleRate;
    cell.valetLabel.text = [NSString stringWithFormat:@"%@", (self.model.withValet? @"YES":@"NO")];
    NSLog(@"TEST %@",self.distance);
    
    if ([GlobalMethods checkInputForWhiteSpaces:self.model.imagePath])
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.parkingImageView setImageWithURL:[NSURL URLWithString:self.model.imagePath]
                                  placeholderImage:[UIImage imageNamed:@"defaultUserIcon"]options:SDWebImageRefreshCached
                       usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            });
    else
        [cell.parkingImageView setImage:[UIImage imageNamed:@"img_no_parking"]];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.model.isBookable)
        return 520.0f;
    
    return 550.0f;
}

@end
