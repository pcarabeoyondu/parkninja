//
//  SavedPaymentCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/5/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "SavedPaymentCell.h"

@implementation SavedPaymentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
