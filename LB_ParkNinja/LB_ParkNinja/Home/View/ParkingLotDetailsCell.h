//
//  ParkingLotDetailsCell.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 01/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingLotDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceBetweenHourlyRateAndSucceedingRate;

//exclusive for NonBookable
@property (weak, nonatomic) IBOutlet UILabel *holidayWeekendRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *overnightRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *motorcycleRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *valetLabel;

//both for NonBookable and Bookable
@property (weak, nonatomic) IBOutlet UILabel *parkingNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkingAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableSlotsLabel;  //used for total slots of non bookable
@property (weak, nonatomic) IBOutlet UILabel *operatingHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *hourlyRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *succeedingRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet UIImageView *parkingImageView;


@end
