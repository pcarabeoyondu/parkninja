//
//  GlobalConstants.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 10/11/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, LOGIN_BTN_ACTION_TAG)
{
    LOGIN_BTN_ACTION_FACEBOOK,
    LOGIN_BTN_ACTION_MANUAL,
    LOGIN_BTN_FORGOT_PASSWORD
};

@interface GlobalConstants : NSObject

extern NSString *const GOOGLE_PLACES_API_KEY;
extern NSString *const FLURRY_ANALYTICS_KEY;
extern NSString *const GOOGLE_CMESSAGING_KEY;

#define SAT @"http://52.74.190.173:8080/parkninja-core/api/"
#define Prod @"http://parkninja.ph/parkninja-core/api/"
#define SIT @"http://52.76.0.172:8080/parkninja-core/api/"

#define SATLink @"http://52.74.190.173:8080/"
#define SITLink @"http://52.76.0.172:8080/"
#define ProdLink @"http://parkninja.ph/"

#define kAPIEndpointHost Prod
#define kAPIEndPointHostLink ProdLink


#define URL_REQUEST_GOOGLE_ROUTE         @"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%@&sensor=true"
#define URL_ADD_CREDIT_CARD              (kAPIEndPointHostLink @"parkninja-core/pay/addCard?clientId=")

#define URL_LOGIN_FACEBOOK               (kAPIEndpointHost @"user/fbLogin")
#define URL_LOGIN                        (kAPIEndpointHost @"user/login")
#define URL_SIGN_UP                      (kAPIEndpointHost @"user/register")
#define URL_ACTIVATE_ACCOUNT             (kAPIEndpointHost @"user/activate")
#define URL_FORGOT_PASSWORD              (kAPIEndpointHost @"user/profile/forgotpassword")

#define URL_UPDATE_PASSWORD              (kAPIEndpointHost @"user/profile/password")
#define URL_UPDATE_PROFILE               (kAPIEndpointHost @"user/profile/update")
#define URL_UPDATE_EMAIL                 (kAPIEndpointHost @"user/profile/email")
#define URL_ACTIVATE_EMAIL               (kAPIEndpointHost @"user/profile/email/activate")


#define URL_PARK_LIST_BOOKABLE           (kAPIEndpointHost @"parking/list")
#define URL_PARK_LIST_NONBOOKABLE        (kAPIEndpointHost @"parking/nonbookable/list")
#define URL_RESERVE_ESTIMATION           (kAPIEndpointHost @"parking/reserve/estimate")

#define URL_CURRENT_SERVER_TIME          (kAPIEndpointHost @"parking/time")
#define URL_PARKING_DETAILS              (kAPIEndpointHost @"parking/details")
#define URL_PARKING_IMAGE                (kAPIEndpointHost @"parking/image")

#define URL_CAR_LIST                     (kAPIEndpointHost @"car/list")
#define URL_CAR_ADD                      (kAPIEndpointHost @"car/add")
#define URL_CAR_UPDATE                   (kAPIEndpointHost @"car/update")
#define URL_CAR_REMOVE                   (kAPIEndpointHost @"car/delete")
#define URL_CAR_VALIDATE                 (kAPIEndpointHost @"car/validateCar")

#define URL_TRANSACTIONS                 (kAPIEndpointHost @"user/transactions")
#define URL_CARD_LIST                    (kAPIEndpointHost @"user/cards")
#define URL_DELETE_TICKET                (kAPIEndpointHost @"/user/ticket/remove")
#define URL_PROCEED_PURCHASE             (kAPIEndpointHost @"parking/reserve/proceed")
#define URL_GET_QR_CODE                  (kAPIEndpointHost @"parking/qrCode")
#define URL_PROMO_CODE_LIST              (kAPIEndpointHost @"parking/promoCode")
#define URL_PROMO_CODE_VERIFY            (kAPIEndpointHost @"parking/promoCode/validate")

#define URL_CANCEL_PARKING               (kAPIEndpointHost @"user/reservation/cancel")
#define URL_REGISTER_GCM                 (kAPIEndpointHost @"user/register/gcm")

#define URL_TERMS                        (kAPIEndPointHostLink @"terms")
#define URL_FAQ                          (kAPIEndPointHostLink @"#/faq")
#define URL_TUTORIAL                     (kAPIEndPointHostLink @"#/home")
#define URL_PRIVACY_POLICY               (kAPIEndPointHostLink @"#/privacypolicy")

#define URL_FACEBOOK                     @"https://www.facebook.com/ParkNinjaPH"
#define URL_TWITTER                      @"https://www.twitter.com/parkninjaph"
#define URL_INSTAGRAM                    @"https://instagram.com/parkninjaph"


//SEGUES
#define SEGUE_SHOW_PARKING_LOT_DETAILS      @"showParkingLotDetails"
#define SEGUE_SHOW_USER_INFORMATION         @"showUserInformation"
#define SEGUE_SHOW_PARKING_SUMMARY          @"showUserParkingSummary"
#define SEGUE_SHOW_PAYMENT_DETAILS          @"showUserPaymentDetails"
#define SEGUE_SHOW_PAYMENT                  @"showPayment"
#define SEGUE_SHOW_ETICKET                  @"showETicket"
#define SEGUE_SHOW_SEARCH_RESULTS           @"showSearchResults"
#define SEGUE_SHOW_ETICKET_FROM_LIST        @"showVCFromTicketList"
#define SEGUE_SHOW_PARKING_LIST             @"showParkingList"
#define SEGUE_SHOW_FREE_ETICKET             @"showFreeETicket"

//PUSH EVENT NAMES
#define PUSH_CHECK_OUT                      @"CHECKOUT"
#define PUSH_NO_SHOW                        @"NO_SHOW"
#define PUSH_MIDNIGHT_CHECKOUT              @"MIDNIGHT_CHECKOUT"

//CONSTANTS
#define kKEYBOARD_HEIGHT 210.0

#define SB_MAIN         @"HomeSB"
#define SB_LOGIN        @"LoginSB"

#define WEB_TERMS       @"showTerms"
#define WEB_FAQ         @"showFAQ"
#define WEB_TUTORIAL    @"showTutorial"
#define WEB_PRIVACY     @"showPrivacy"

#define WEB_SHOW        @"showWebView"

#define SERVER_ERROR_MESSAGE            @"Error retrieving information from server."
#define SERVER_ERROR_KEY                @"message"
#define SERVER_ERROR_TITLE              @"Server Error"

#define TITLE_UPDATE                    @"Update"
#define TITLE_VEHICLES                  @"Vehicles"

#define MESSAGE_FIELDS_INCOMPLETE       @"All fields are required."
#define MESSAGE_MOBILE_INVALID          @"Mobile number is invalid."
#define MESSAGE_EMAIL_INVALID           @"Email is invalid."
#define MESSAGE_CAR_UPDATE_SUCCESS      @"Vehicle has been successfully updated."
#define MESSAGE_CAR_ADDED_SUCCESS       @"Vehicle has been successfully added."
#define MESSAGE_CAR_REMOVED_SUCCESS     @"Vehicle has been successfully removed."

#define MESSAGE_TIME_INVALID            @"Invalid time was set."


#define MESSAGE_PASSWORD_INVALID        @"Password is invalid. Kindly check your input."

#define MESSAGE_UPDATE_SUCCESS          @"Profile has been successfully updated."

//PINS
#define PIN_RED         @"pin_red"
#define PIN_GREEN       @"pin_green"
#define PIN_ORANGE      @"pin_orange"
#define PIN_YELLOW      @"pin_yellow"
#define PIN_UNBOOKABLE  @"pin_unbookable"

//iOS Version
#define GET_OS_VERSION    ([[[UIDevice currentDevice] systemVersion] floatValue])

@end
