//
//  ParkingReserveEstimationModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionModel.h"

@interface ParkingReserveEstimationModel : NSObject

@property (strong, nonatomic) NSString *email;
@property (assign, nonatomic) BOOL earlyBird;
@property (strong, nonatomic) NSString *promoCode;

@property (assign, nonatomic) NSInteger ratePrice;

@property (assign, nonatomic) NSInteger convenienceFee;

- (id)initParkingEstimationWithDictionary:(NSDictionary *)dictionary;

@end
