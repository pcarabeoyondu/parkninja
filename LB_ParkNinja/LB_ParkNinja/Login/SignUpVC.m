//
//  SignUpVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SignUpVC.h"
#import <MBProgressHUD/MBProgressHUD.h>

#import "UserSignUpModel.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "UserModel.h"

#import "TermsAndConditionsVC.h"
#import "HomeVC.h"

#import "AppDelegate.h"

#define MAX_LENGTH 11
#define MAX_LENGTH_SURNAME 2

typedef NS_ENUM(NSInteger, SIGN_UP_TEXTFIELD_INDEX)
{
    TF_INDEX_FULL_NAME,
    TF_INDEX_EMAIL,
    TF_INDEX_PASSWORD,
    TF_INDEX_MOBILE_NUM,
    TF_INDEX_BIRTHDAY
};

@interface SignUpVC ()

@property (weak, nonatomic) IBOutlet UITableView *signUpTableView;
@property (strong, nonatomic) UserSignUpModel *userModel;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) IBOutlet UIView *pickerContainerView;

@end

@implementation SignUpVC
{
    BOOL passwordSecurity;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.userModel == nil)
        self.userModel = [[UserSignUpModel alloc] init];
    
    passwordSecurity = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)showActivationPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Activation Code"
                                                                   message:@"Please enter the activation code we sent to your email."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *code = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     [self requestActivateAccountWithCode:code];
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Activation Code";
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showDatePickerView
{
    [self.view endEditing:YES];
    
    self.pickerContainerView.hidden = NO;
}

- (void)resignAllTextFields
{
    [self.view endEditing:YES];
}

- (BOOL)checkAllUserInput
{
    BOOL isValid = YES;
    
    NSString *message = MESSAGE_FIELDS_INCOMPLETE;
    
    if ((![GlobalMethods checkInputForWhiteSpaces:self.userModel.fullName] || ![GlobalMethods checkInputForWhiteSpaces:self.userModel.email]||
         ![GlobalMethods checkInputForWhiteSpaces:self.userModel.password] ||
         ![GlobalMethods checkInputForWhiteSpaces:self.userModel.mobileNum]))
    {
        isValid = NO;
    }
    
    else if (![GlobalMethods NSStringIsValidEmail:self.userModel.email])
    {
        message = @"Invalid email";
        isValid = NO;
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:message];
    
    if (!isValid)
        [self presentViewController:alert animated:YES completion:nil];
    
    return isValid;
}

- (void)signUpUserDetails
{
    [self resignAllTextFields];
    
    if ([self checkAllUserInput] && [GlobalMethods isConnected])
    {
        [self requestUserDetails];
    };
}

#pragma mark - UIDatePicker
- (IBAction)datePickerAction:(id)sender
{
   // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
}

#pragma mark - API Request
- (void)requestActivateAccountWithCode:(NSString *)code
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *activateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:self.userModel.email, @"email", code, @"activationKey", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_ACTIVATE_ACCOUNT withParameters:activateDictionary withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        [GlobalMethods saveUserWithInfo:responseDictionary];
        
        if ([[[GlobalMethods getUser] willProceedToPayment] boolValue])
        {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RequestTransitionToPayment" object:nil userInfo:nil];
        }
        
        else
        {
            AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [app setRootViewControllerWithSB:SB_MAIN];
        }
        
        [self requestRegisterGCM];
        
        [GlobalMethods saveUserType:FALSE];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {

        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:serverError])
              errorStr = customDescription;
              
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:errorStr
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         
                                                         [self showActivationPopUp];
                                                     }];
        

        [alert addAction:ok];

        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestUserDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:self.userModel.email, @"email", self.userModel.password, @"password", self.userModel.fullName, @"fullName", self.userModel.mobileNum, @"mobileNo", [NSNumber numberWithInteger:self.userModel.birthday], @"birthday", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_SIGN_UP withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        [self showActivationPopUp];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {

        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
         [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - GCM
- (void)requestRegisterGCM
{
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [[AppDelegate appDelegate] registrationToken], @"gcmToken", [[GlobalMethods getUser] email], @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_REGISTER_GCM withParameters:userInfo withCompletionBlockSuccess: ^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
 
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        NSLog(@"SERVER ERROR: %@", serverError);
    }];
    
}

#pragma mark - UIButtonAction
- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissPickerView:(id)sender
{
    NSDate *today = [NSDate date]; // it will give you current date
    NSComparisonResult result;
    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
    
    result = [today compare:self.datePicker.date]; // comparing two dates
    
    if (result == NSOrderedAscending)
    {
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:@"Invalid Date" message:@"Kindly check your input date."];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *dateString = [dateFormat stringFromDate:self.datePicker.date];
        NSString *stringDate = [NSString stringWithFormat:@"%@", dateString];
        
        NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
        [dateFormat1 setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat1 setDateFormat:@"MM/dd/yyyy"];
        NSDate *selectedDate = [dateFormat1 dateFromString:stringDate];
        
        NSCalendar *calendar = [[NSCalendar alloc]
                                initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setHour:8];
        
        NSDate *dateInput = [calendar dateByAddingComponents:components toDate:selectedDate options:0];
        
        long timestamp = ([dateInput timeIntervalSince1970] * 1000) + 14400000;
        
        self.userModel.birthday = timestamp;
        
        [self.signUpTableView reloadData];
        self.pickerContainerView.hidden = YES;
    }
}

- (IBAction)cancelPickerView:(id)sender
{
    self.pickerContainerView.hidden = YES;
}
- (IBAction)showPassword:(id)sender
{
    passwordSecurity = NO;
    
    [self.signUpTableView reloadData];
}

#pragma mark - UITableViewCell
- (SignUpDetailsCell *)setSignUpDetailsWithRow:(NSInteger)row
{
    SignUpDetailsCell *cell = [self.signUpTableView dequeueReusableCellWithIdentifier:@"SignUpDetailsCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SignUpDetailsCell" owner:self options:nil];
        cell = (SignUpDetailsCell *)nibArray[0];
    }
    
    cell.delegate = self;
    [cell initDetails];
    [cell.signUpButton addTarget:self action:@selector(signUpUserDetails) forControlEvents:UIControlEventTouchUpInside];
    [cell.calendarButton addTarget:self action:@selector(showDatePickerView) forControlEvents:UIControlEventTouchUpInside];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSString *formattedDate = [dateFormatter stringFromDate:self.datePicker.date];
    
    [cell.birthdayButton setTitle:formattedDate forState: UIControlStateNormal];
    
    [cell setSignUpDetailsWithModel:self.userModel];
    cell.passwordTextField.secureTextEntry = passwordSecurity;
    cell.showPasswordButton.hidden = !passwordSecurity;
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setSignUpDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.signUpTableView.frame.size.height;
}


#pragma mark - UITextFieldDelegate
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _signUpTableView.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    _signUpTableView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setViewMovedUp:YES];
    
    NSInteger position = UITableViewScrollPositionNone;
    
    switch (textField.tag)
    {
        case TF_INDEX_FULL_NAME:
        {
            position = UITableViewScrollPositionTop;
            break;
        }
            
        case TF_INDEX_EMAIL:
        {
            position = UITableViewScrollPositionMiddle;
            break;
        }
            
        case TF_INDEX_MOBILE_NUM:
        case TF_INDEX_BIRTHDAY:
        {
            position = UITableViewScrollPositionBottom;
            break;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_signUpTableView scrollToRowAtIndexPath:rowIndexPath atScrollPosition:position animated:YES];
    });
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    NSString *userInput = textField.text;
    
    switch (textField.tag)
    {
        case TF_INDEX_FULL_NAME:
        {
            self.userModel.fullName = userInput;
            break;
        }
            
        case TF_INDEX_EMAIL:
        {
            self.userModel.email = userInput;
            break;
        }
            
        case TF_INDEX_PASSWORD:
        {
            self.userModel.password = userInput;
            break;
        }
    
        case TF_INDEX_MOBILE_NUM:
        {
            self.userModel.mobileNum = userInput;
            break;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == TF_INDEX_MOBILE_NUM)
    {
        if (textField.text.length >= MAX_LENGTH && range.length == 0)
            return NO;
        
        else
            return YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    
    UIResponder *nextResponder = [self.view viewWithTag:nextTag];
    
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - SignUpDetailsCellDelegate
- (void)showTermsAndConditionsPage
{
    UIStoryboard *loginSB = [UIStoryboard storyboardWithName:@"LoginSB" bundle:nil];
    TermsAndConditionsVC *vc = [loginSB instantiateViewControllerWithIdentifier:@"TermsAndConditionsVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
