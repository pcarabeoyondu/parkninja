//
//  LocationManager.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationManagerDelegate;

@interface LocationManager : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) id <LocationManagerDelegate> delegate;

+ (LocationManager *)sharedInstance;
- (void)initLocationManager;

@end

@protocol LocationManagerDelegate <NSObject>

- (void)getCurrentUserLocation:(CLLocation *)location;

@end