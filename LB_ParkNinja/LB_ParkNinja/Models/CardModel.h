//
//  CardModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/8/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardModel : NSObject

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *maskedNum;
@property (strong, nonatomic) NSString *token;

@property (assign, nonatomic) NSInteger status;
@property (assign, nonatomic) NSInteger cardId;

- (id)initCardModelWithDictionary:(NSDictionary *)dictionary;

@end
