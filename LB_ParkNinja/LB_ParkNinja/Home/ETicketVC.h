//
//  ETicketVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"
#import "ParkingReserveEstimationModel.h"
#import "TransactionModel.h"
#import "ParkingLotModel.h"

@interface ETicketVC : UIViewController

@property (strong, nonatomic) TransactionModel *transactionModel;
@property (assign, nonatomic) BOOL isNavigationBarHidden;
@property (strong, nonatomic) ParkingLotModel *parkingModel;

@end
