 //
//  VehiclesVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "VehiclesVC.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AppDelegate.h"
#import "VehicleCell.h"

#import "GlobalMethods.h"
#import "User.h"
#import "UserModel.h"

#import "CarModel.h"
#import "GlobalConstants.h"
#import "HttpManagerBlocks.h"

@interface VehiclesVC ()

@property (weak, nonatomic) IBOutlet UITableView *vehiclesTableView;
@property (strong, nonatomic) NSMutableArray *carListArray;

@end

@implementation VehiclesVC
{
    User *user;
    UserModel *userModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    if ([GlobalMethods isConnected])
        [self requestCarDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.vehiclesTableView addSubview:refreshControl];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self requestCarDetails];
    
    [refreshControl endRefreshing];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Vehicles";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAddCarPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add Car"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *carMake = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     NSString *plateNo = ((UITextField *)[alert.textFields objectAtIndex:1]).text;
                                                     NSString *model = ((UITextField *)[alert.textFields objectAtIndex:2]).text;
                                                     NSString *color = ((UITextField *)[alert.textFields objectAtIndex:3]).text;
                                                     
                                                     if ([GlobalMethods checkInputForWhiteSpaces:plateNo] && [GlobalMethods checkInputForWhiteSpaces:carMake])
                                                     {
                                                         CarModel *carModel = [[CarModel alloc] init];
                                                         carModel.plateNum = plateNo;
                                                         carModel.make = carMake;
                                                         
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:model])
                                                             carModel.model = model;
                                                         else
                                                             carModel.model = @"";
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:color])
                                                             carModel.color = color;
                                                         else
                                                             carModel.color = @"";
                                                         
                                                         [self requestAddCarWithModel:carModel];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:MESSAGE_FIELDS_INCOMPLETE];
                                                         [self presentViewController:alert animated:YES completion:nil];
                                                         
                                                     }
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Brand";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Plate Number";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Model";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Color";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showRemoveCarPopUp:(UIButton *)sender
{
    CarModel *modelSelected = [self.carListArray objectAtIndex:sender.tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Car" message:@"Are you sure you want to remove this vehicle?" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                      [self requestRemoveCarWithModel:modelSelected];
                                                     
                                                 }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                                          handler:^(UIAlertAction *action) {
                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                          }];
                           
    [alert addAction:cancel];
    [alert addAction:done];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showUpdateCarPopUp:(UIButton *)sender
{
    CarModel *modelSelected = [self.carListArray objectAtIndex:sender.tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Car"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *carMake = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     NSString *plateNo = ((UITextField *)[alert.textFields objectAtIndex:1]).text;
                                                     NSString *model = ((UITextField *)[alert.textFields objectAtIndex:2]).text;
                                                     NSString *color = ((UITextField *)[alert.textFields objectAtIndex:3]).text;
                                                     
                                                     if ([GlobalMethods checkInputForWhiteSpaces:plateNo] && [GlobalMethods checkInputForWhiteSpaces:carMake])
                                                     {
                                                         CarModel *carModel = [[CarModel alloc] init];
                                                         carModel.plateNum = plateNo;
                                                         carModel.make = carMake;
                                                         carModel.carId = modelSelected.carId;
                                                         
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:model])
                                                             carModel.model = model;
                                                         else
                                                             carModel.model = @"";
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:color])
                                                             carModel.color = color;
                                                         else
                                                             carModel.color = @"";
                                                         
                                                        [self requestUpdateCarWithModel:carModel withTag:sender.tag];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:MESSAGE_FIELDS_INCOMPLETE];
                                                         [self presentViewController:alert animated:YES completion:nil];
                                                         
                                                     }
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Brand";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
        textField.text = modelSelected.make;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Plate Number";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
        textField.text = modelSelected.plateNum;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Model";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
        
        if(![modelSelected.model isEqual:[NSNull null]])
            textField.text = modelSelected.model;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Color";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
        
        if(![modelSelected.color isEqual:[NSNull null]])
            textField.text = modelSelected.color;

        
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - API REQUEST
- (void)requestCarDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        self.carListArray = nil;
        
        self.carListArray = [NSMutableArray array];
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            CarModel *carModel = [[CarModel alloc] initWithCarDictionary:(NSDictionary *)array];
            [self.carListArray addObject:carModel];
        }
        
        [GlobalMethods hideProgressHUD];
        
        [self.vehiclesTableView reloadData];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError)
    {
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestAddCarWithModel:(CarModel *)model
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", model.make, @"make", model.model, @"model", model.color, @"color", model.plateNum, @"plateNo", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_ADD withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [self requestCarDetails];
        
        [GlobalMethods hideProgressHUD];
        
        [self.vehiclesTableView reloadData];
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_VEHICLES message:MESSAGE_CAR_ADDED_SUCCESS];
        [self presentViewController:alert animated:YES completion:nil];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestUpdateCarWithModel:(CarModel *)model withTag:(NSUInteger)tag
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:model.carId], @"id", model.make, @"make", model.model, @"model", model.color, @"color", model.plateNum, @"plateNo", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_UPDATE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [self.carListArray replaceObjectAtIndex:tag withObject:model];
        
        [GlobalMethods hideProgressHUD];
        
        [self.vehiclesTableView reloadData];
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_VEHICLES message:MESSAGE_CAR_UPDATE_SUCCESS];
        [self presentViewController:alert animated:YES completion:nil];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestRemoveCarWithModel:(CarModel *)model
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:model.carId], @"id", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_REMOVE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [self requestCarDetails];
        
        [GlobalMethods hideProgressHUD];
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:TITLE_VEHICLES message:MESSAGE_CAR_REMOVED_SUCCESS];
        [self presentViewController:alert animated:YES completion:nil];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - UITableViewCell
- (VehicleCell *)setCarDetailsWithRow:(NSInteger)row
{
    VehicleCell *cell = [self.vehiclesTableView dequeueReusableCellWithIdentifier:@"cardetails"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"VehicleCell" owner:self options:nil];
        cell = (VehicleCell *)nibArray[0];
    }
 
    cell.borderView.layer.masksToBounds = YES;
    cell.borderView.layer.borderColor =[UIColor lightGrayColor].CGColor;
    cell.borderView.layer.borderWidth = 0.7;
    
    CarModel *model = [self.carListArray objectAtIndex:row];
    cell.carDetailsLabel.text = [NSString stringWithFormat:@"%@ (%@)", model.make, model.plateNum];
   
    cell.editButton.tag = row;
    cell.deleteButton.tag = row;
    
    [cell.editButton addTarget:self action:@selector(showUpdateCarPopUp:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton addTarget:self action:@selector(showRemoveCarPopUp:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.carListArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.carListArray count])
        return [tableView dequeueReusableCellWithIdentifier:@"addnewcar" forIndexPath:indexPath];
    
        return  [self setCarDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.vehiclesTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [self.carListArray count])
        [self showAddCarPopUp];
}

@end
