//
//  ParkingListManager.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 7/13/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ParkingListManager.h"

static ParkingListManager *_pLManager;

@implementation ParkingListManager

+ (ParkingListManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _pLManager = [[ParkingListManager alloc] init];
    });
    return _pLManager;
}

- (void)requestAllParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion
{
    NSMutableArray *allParkingList = [NSMutableArray array];
    [self requestBookableParkingList:^(BOOL successful, NSArray*parkingList, NSError *error) {
        
        BOOL good = successful;
        NSError *bookableError = error;
        [allParkingList addObjectsFromArray:parkingList];
        [self requestNonBookableParkingList:^(BOOL successful, NSArray*parkingList, NSError *error) {
            
            [allParkingList addObjectsFromArray:parkingList];
            completion((good || successful),[allParkingList copy], [self mergerBookableError:bookableError andNonBookableError:error]);
            
        }];
        
    }];
}

- (void)requestNonBookableParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion
{
    [self requestParkingListFromURL:URL_PARK_LIST_NONBOOKABLE withParameters:nil andCompletion:^(BOOL successful, NSArray *parkingList, NSError *error) {
        for (ParkingLotModel *parking in parkingList) {
            parking.isBookable = NO;
        }
        completion(successful,parkingList,error);
    }];
}
- (void)requestBookableParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion
{
    [self requestParkingListFromURL:URL_PARK_LIST_BOOKABLE withParameters:nil andCompletion:^(BOOL successful, NSArray *parkingList, NSError *error) {
        for (ParkingLotModel *parking in parkingList) {
            parking.isBookable = YES;
        }
        completion(successful,parkingList,error);
    }];
}

#pragma mark - Private API
- (NSError *)mergerBookableError:(NSError *)bookableError andNonBookableError:(NSError *)nonBookableError
{
    //if both are nil, return return nil.
    if (!bookableError && !nonBookableError) return NULL;
    
    //if one is nil, return the other that's not.
    if (bookableError && !nonBookableError) return bookableError;
    if (!bookableError && nonBookableError) return nonBookableError;
    
    //if both non nil, merge localized description but use bookable domain and code :p
    NSString *nonBookableDesc = nonBookableError.localizedDescription;
    NSString *bookableDesc = bookableError.localizedDescription;
    NSString *mergeDesc = [NSString stringWithFormat:@"Bookable: %@ \nNonBookable: %@",bookableDesc,nonBookableDesc];
    
    NSMutableDictionary *userInfo = [bookableError.userInfo mutableCopy];
    [userInfo setObject:mergeDesc forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:bookableError.domain code:bookableError.code userInfo:userInfo];
    return error;
}

- (void)requestParkingListFromURL:(NSString *)url withParameters:(NSDictionary *)params andCompletion:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion
{
    NSMutableArray *parkingList = [NSMutableArray array];
    [[HttpManagerBlocks sharedInstance] getFromUrl:url withParameters:params withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        for (NSDictionary *data in [responseDictionary objectForKey:@"results"])
        {
            ParkingLotModel *parkingModel = [[ParkingLotModel alloc] initWithParkingLotDictionary:data];
            [parkingList addObject:parkingModel];
        }
        
        completion(YES,[parkingList copy], NULL);
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        NSString *errorString = [GlobalMethods checkInputForWhiteSpaces:serverError] ? serverError : customDescription;
        
        NSError *errorToSend = error;
        if ([GlobalMethods checkInputForWhiteSpaces:errorString]) {
            NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
            [userInfo setObject:errorString forKey:NSLocalizedDescriptionKey];
            errorToSend =[NSError errorWithDomain:error.domain code:error.code userInfo:userInfo];
        }
        
        completion(NO,[parkingList copy], errorToSend);
    }];
}

@end
