//
//  SignUpDetailsCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SignUpDetailsCell.h"
#import <CoreText/CoreText.h>
#import "GlobalMethods.h"

@implementation SignUpDetailsCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initDetails
{
    NSMutableAttributedString *preString = [[NSMutableAttributedString alloc] initWithString:@"By proceeding, you also agree to "];
    [preString addAttribute:(NSString *)NSForegroundColorAttributeName
                        value:[GlobalMethods colorFromHexString:@"#6D6D6D"]
                        range:(NSRange){0,[preString length]}];
    
    NSMutableAttributedString *parkNinja = [[NSMutableAttributedString alloc] initWithString:@"Park  Ninja's "];
    [parkNinja addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#6D6D6D"]
                      range:(NSRange){0,[parkNinja length]}];

   [parkNinja addAttribute:(NSString *)kCTFontAttributeName value:[UIFont fontWithName:@"FSElliotPro-Bold" size:17] range:(NSRange){0,[parkNinja length]}];
    
    NSMutableAttributedString *termsString = [[NSMutableAttributedString alloc] initWithString:@"Terms of Service and Privacy Policy"];
    
    [termsString addAttribute:(NSString *)kCTUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                      range:(NSRange){0,[termsString length]}];
    
    [termsString addAttribute:(NSString *)NSForegroundColorAttributeName
                        value:[GlobalMethods colorFromHexString:@"#0C9BAA"]
                        range:(NSRange){0,[termsString length]}];
    
    
    NSMutableAttributedString *comString = [[NSMutableAttributedString alloc] initWithAttributedString:preString];
    [comString appendAttributedString:parkNinja];
    [comString appendAttributedString:termsString];
    
    self.termsAndConditionLabel.attributedText = comString;    
    
    //add gesture recognizer to label
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConditionsLabelTapped)];
    
    [self.termsAndConditionLabel addGestureRecognizer:singleTap];
}

- (void)setSignUpDetailsWithModel:(UserSignUpModel *)model
{
    self.firstNameTextField.text = model.fullName;
    self.emailTextField.text = model.email;
    self.passwordTextField.text = model.password;
    self.confirmPasswordTextField.text = model.confirmPassword;
    self.mobileNumTextField.text = model.mobileNum;    
}

- (void)termsAndConditionsLabelTapped
{
    [self.delegate showTermsAndConditionsPage];
}

@end
