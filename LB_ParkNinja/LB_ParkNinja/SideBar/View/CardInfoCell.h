//
//  CardInfoCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/11/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardLabel;

@end
