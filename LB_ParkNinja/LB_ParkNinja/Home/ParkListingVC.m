//
//  ParkListingVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkListingVC.h"
#import "AppDelegate.h"

#import "ParkingInfoCell.h"
#import "HomeVC.h"
#import "HomeNC.h"
#import "ParkingLotDetailsVC.h"
#import "GlobalConstants.h"
#import "ParkingLotModel.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "GlobalMethods.h"
#import "ParkingListManager.h"

@interface ParkListingVC ()

@property (weak, nonatomic) IBOutlet UITableView *parkingListTableView;
@property (strong, nonatomic) NSMutableArray *parkingList;
@property (strong, nonatomic) NSMutableArray *searchResultsMArray;
@property (strong, nonatomic) ParkingLotModel *selectedModel;


@end

@implementation ParkListingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.parkingList = [NSMutableArray array];
    
    if ([GlobalMethods isConnected])
        [self requestAllParkingList];
    
    [self setupNavigationItem];
    [self setupNavigationBar];
    
    //moved this to viewdidload from viewwillappear because refresh control gets duplicated when you move to another view controller and back to this list causing the refresh to be called n-times where n = the number of times this view appeared.
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.parkingListTableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self requestAllParkingList];
    
    [refreshControl endRefreshing];
}

#pragma mark - Private Method
- (void)setupNavigationItem
{
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Pick a Parking Spot";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SHOW_PARKING_LOT_DETAILS])
    {
        ParkingLotDetailsVC *vc = (ParkingLotDetailsVC *)segue.destinationViewController;
        vc.model = self.selectedModel;
        vc.location = self.location;
        return;
    }
}

#pragma mark - API Request
- (void)requestAllParkingList
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    [[ParkingListManager sharedInstance] requestAllParkingList:^(BOOL successful, NSArray *parkingList, NSError *error) {
        if (successful) {
            self.parkingList = [parkingList mutableCopy];
            [self.parkingListTableView reloadData];
            [GlobalMethods hideProgressHUD];
        }else {
            UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:error.localizedDescription];
            [self presentViewController:alert animated:YES completion:nil];
            [GlobalMethods hideProgressHUD];
            
            NSLog(@"SERVER ERROR: %@", error.localizedDescription);
        }
    }];
}

#pragma mark - ParkingInfoCell
- (ParkingInfoCell *)setParkingDetailsWithRow:(NSInteger)row
{
    ParkingInfoCell *cell = [self.parkingListTableView dequeueReusableCellWithIdentifier:@"ParkingInfoCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingInfoCell" owner:self options:nil];
        cell = (ParkingInfoCell *)nibArray[0];
    }
    
    ParkingLotModel *model = [self.parkingList objectAtIndex:row];
    [cell.mainAddressLabel setText:model.location];
    [cell.detailsAddressLabel setText:model.location];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [GlobalMethods colorFromHexString:@"#F1F7F7"];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.parkingList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedModel = [self.parkingList objectAtIndex:indexPath.row];

    [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_LOT_DETAILS sender:self];
    
    [self.parkingListTableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
