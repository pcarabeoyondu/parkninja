//
//  CardModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/8/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "CardModel.h"

@implementation CardModel

- (id)initCardModelWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if ([dictionary objectForKey:@"cardStatus"] == (id) [NSNull null])
            self.status = 1;
        else
            self.status = [[dictionary objectForKey:@"cardStatus"] integerValue];
        
        self.cardId = [[dictionary objectForKey:@"id"] integerValue];
       
        
        
        self.maskedNum = [dictionary objectForKey:@"maskedCardNo"];
        self.type = [dictionary objectForKey:@"cardType"];
        //self.token = [dictionary objectForKey:@"token"];
    }
    
    return self;
}

@end
