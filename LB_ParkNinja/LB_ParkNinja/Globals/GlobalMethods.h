//
//  GlobalMethods.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/13/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "User.h"
#import "AppDelegate.h"
#import "ParkingLotModel.h"
#import <Reachability/Reachability.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface GlobalMethods : NSObject

+ (UIAlertController *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (void)saveUserWithInfo:(NSDictionary *)userInfo;
+ (User *)getUser;
+ (void)logout;

+ (void)saveUserWithImage:(UIImage *)image;
+ (void)saveUserType:(BOOL)isGuest;
+ (void)savePaymentStatus:(BOOL)willProceed;
+ (void)didReceivePushNotif:(BOOL)received;
+ (void)willNavigateToParking:(BOOL)navigate lat:(float)latitude long:(float)longitude;

+ (BOOL)isConnected;

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL)checkInputForWhiteSpaces:(NSString *)inputString;
+ (UIColor *)colorFromHexString:(NSString *)hexString;


+ (MBProgressHUD *)showProgressHUD;
+ (void)hideProgressHUD;
@end
