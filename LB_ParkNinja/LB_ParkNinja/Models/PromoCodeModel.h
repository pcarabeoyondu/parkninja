//
//  PromoCodeModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/9/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromoCodeModel : NSObject

@property (assign, nonatomic) NSInteger promoCodeId;
@property (assign, nonatomic) NSInteger discountRateCurrency;
@property (assign, nonatomic) NSInteger discountRatePercentage;
@property (assign, nonatomic) NSInteger usesPerUser;
@property (assign, nonatomic) NSInteger available;
@property (strong, nonatomic) NSString *deletedCodeParkingLots;
@property (assign, nonatomic) BOOL isDiscountRateCurrency;
@property (assign, nonatomic) BOOL isFreeParking;
@property (copy, nonatomic)   NSString *codeParkingLot;
@property (copy, nonatomic)   NSString *code;
@property (copy, nonatomic)   NSString *status;

- (id)initPromoCodeWithDictionary:(NSDictionary *)dictionary;

@end
