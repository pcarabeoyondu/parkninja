//
//  FailToArriveView.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 4/2/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FailToArriveView : UIView

@property (weak, nonatomic) IBOutlet UILabel *contactDetailsLabel;

- (void)initDetails;

@end
