//
//  SideBarVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SideBarVC.h"

#import "ProfileVC.h"
#import "MyTicketsVC.h"
#import "TermsAndConditionsVC.h"
#import "FAQVC.h"
#import "AboutVC.h"
#import "HomeVC.h"
#import "ProfileImageCell.h"
#import "User.h"
#import "UserModel.h"
#import "GlobalMethods.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleCloudMessaging/GoogleCloudMessaging.h>

typedef NS_ENUM (NSInteger, SIDE_CELL_INDEX)
{
    SIDE_PROFILE = 0,
    SIDE_HOME,
    SIDE_TICKETS,
    SIDE_PAYMENTS,
    SIDE_VEHICLES,
    SIDE_ABOUT,
    SIDE_LOGOUT
};

typedef NS_ENUM (NSInteger, SIDE_GUEST_INDEX)
{
    SIDE_GUEST_PROFILE = 0,
    SIDE_GUEST_HOME,
    SIDE_GUEST_ABOUT,
    SIDE_GUEST_LOGIN
};

@interface SideBarVC ()

@property (strong, nonatomic) IBOutlet UITableView *sideBarTableView;

@end

@implementation SideBarVC
{
    User *user;
    UserModel *userModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    [userModel initWithUserProfileImageData:user];
    [self.sideBarTableView reloadData];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableViewCell
- (ProfileImageCell *)setProfileImageCellWithRow:(NSInteger)row
{
    ProfileImageCell *cell = [self.sideBarTableView dequeueReusableCellWithIdentifier:@"profile"];

    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ProfileImageCell" owner:self options:nil];
        cell = (ProfileImageCell *)nibArray[0];
    }
    
    if ([user.isGuest boolValue])
    {
        cell.nameLabel.text = @"Guest";
        cell.profileImageView.image = [UIImage imageNamed:@"img_default_profile"];
        cell.proceedBtn.hidden = YES;
    }
    
    else
    {
        cell.nameLabel.text = [NSString stringWithFormat:@"%@", user.fullName];
        
        if ([user.displayImage length] > 0)
            cell.profileImageView.image = [UIImage imageWithData:user.displayImage];
        else
            cell.profileImageView.image = [UIImage imageNamed:@"img_default_profile"];
        
        
        cell.proceedBtn.hidden = FALSE;
    }
    
    cell.profileImageView.layer.cornerRadius = 50.0f;
    cell.profileImageView.layer.masksToBounds = YES;
    
    
    
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([user.isGuest boolValue])
        return 4;
    
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"profile";
    
    
    if ([user.isGuest boolValue])
    {
        switch (indexPath.row)
        {
        
            case SIDE_GUEST_PROFILE:
            {
                return [self setProfileImageCellWithRow:indexPath.row];
            }
                
            case SIDE_GUEST_HOME:
            {
                cellIdentifier = @"home";
                break;
            }
                
            case SIDE_GUEST_ABOUT:
            {
                cellIdentifier = @"about";
                break;

            }
                
            case SIDE_GUEST_LOGIN:
            {
                cellIdentifier = @"login";
                break;
            }
        }
    }
    
    else
    {
        switch (indexPath.row)
        {
            case SIDE_PROFILE:
            {
                return [self setProfileImageCellWithRow:indexPath.row];
            }
        
            case SIDE_HOME:
            {
                cellIdentifier = @"home";
                break;
            }

            case SIDE_TICKETS:
            {
                cellIdentifier = @"tickets";
                break;
            }
            case SIDE_PAYMENTS:
            {
                cellIdentifier = @"payment";
                break;
            }
            case SIDE_VEHICLES:
            {
                cellIdentifier = @"vehicles";
                break;
            }
        
            case SIDE_ABOUT:
            {
                cellIdentifier = @"about";
                break;
            }
                
            case SIDE_LOGOUT:
            {
                cellIdentifier = @"logout";
                break;
            }
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [GlobalMethods colorFromHexString:@"#F1F7F7"];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 200.0f;
    
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (([user.isGuest boolValue]) && indexPath.row == SIDE_GUEST_LOGIN)
    {
        [GlobalMethods logout];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate setRootViewControllerWithSB:@"LoginSB"];
    }
    
    else if ((![user.isGuest boolValue]) && indexPath.row == SIDE_LOGOUT)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to log out?" preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *done = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDestructive
                                                     handler:^(UIAlertAction *action) {
                                                         
                                                         [GlobalMethods logout];
                                                         
                                                         FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                                         [loginManager logOut];
                                                         
                                                         [FBSDKAccessToken setCurrentAccessToken:nil];
                                                         
                                                         [[GCMService sharedInstance] disconnect];
                                                         
                                                     }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        
        [alert addAction:done];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self.sideBarTableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
