//
//  HttpManagerBlocks.m
//  Blocks
//
//  Created by Mailyn Sajorda on 3/18/15.
//  Copyright (c) 2015 egg.com. All rights reserved.
//

#import "HttpManagerBlocks.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "NSDictionary+NSNull.h"
#import "GlobalConstants.h"

@implementation HttpManagerBlocks

+ (HttpManagerBlocks *)sharedInstance {
    static dispatch_once_t pred;
    static HttpManagerBlocks *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[HttpManagerBlocks alloc] init];
    });
    return shared;
}

#pragma mark - Reachability Status

- (BOOL) isConnected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
        NSLog(@"NO INTERNET CONNECTION");
    
    return networkStatus != NotReachable;
}

#pragma mark - HTTP Requests
- (void)getTimeFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(timeSuccess)success failure:(failure)failure
{
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n GET REQUEST : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    
    [manager.requestSerializer setValue:@"Bearer d6460933-68e4-4905-9143-368823a4f8b5" forHTTPHeaderField:@"Authorization"];
    
    [manager GET:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *timeResponse = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", timeResponse);

        success(timeResponse);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        
        NSString *serverError = nil;
        
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 400 || statusCode == 500)
        {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
            
            serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        }
        else
        {
            serverError = error.localizedDescription;
        }
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
    
}

- (void)getFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure
{
    // Check for Internet Connection
    if (![self isConnected])
    {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n GET REQUEST : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    
    [manager.requestSerializer setValue:@"Bearer d6460933-68e4-4905-9143-368823a4f8b5" forHTTPHeaderField:@"Authorization"];
    
    [manager GET:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError * error = nil;
        
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options: NSJSONReadingMutableContainers error: &error]];
        
        responseDictionary = [self removeNullValues:responseDictionary];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", responseDictionary);
        
        int responseCode = [responseDictionary[@"response_code"] intValue];
        success(responseDictionary, responseCode);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@", errorResponse);
        
        NSError *errorDict = nil;
        
        NSString *serverError = nil;
        
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 400 || statusCode == 500)
        {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
            
            serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        }
        else
        {
            serverError = error.localizedDescription;
        }
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
    }];
    
}

- (void)postToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure {
    
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n POST : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    // [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager.requestSerializer setValue:@"Bearer d6460933-68e4-4905-9143-368823a4f8b5" forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"PARAMETERS: %@", parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError * error = nil;
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &error]];
        
        responseDictionary = [self removeNullValues:responseDictionary];
        
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", responseDictionary);
        
        int responseCode = [responseDictionary[@"response_code"] intValue];
        success(responseDictionary, responseCode);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        
        NSString *serverError = nil;
    
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 400 || statusCode == 500)
        {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
            
            serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        }
        else
        {
          serverError = error.localizedDescription;
        }
        
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
}

- (void)postPurchaseParkingToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(timeSuccess)success failure:(failure)failure
{    
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n POST : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    // [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager.requestSerializer setValue:@"Bearer d6460933-68e4-4905-9143-368823a4f8b5" forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"PARAMETERS: %@", parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *response = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", response);
        
        success(response);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        
        NSString *serverError = nil;
        
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 400 || statusCode == 500)
        {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
            
            serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        }
        else
        {
            serverError = error.localizedDescription;
        }
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
}

- (void)postQRCodeToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(imageSuccess)success failure:(failure)failure
{
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n POST : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    // [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager.requestSerializer setValue:@"Bearer d6460933-68e4-4905-9143-368823a4f8b5" forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"PARAMETERS: %@", parameters);
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *data = (NSData *)responseObject;
        UIImage *image = [UIImage imageWithData:data];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", data);
        
        success(image);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        
        NSString *serverError = nil;
        
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 400 || statusCode == 500)
        {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
            
            serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        }
        else
        {
            serverError = error.localizedDescription;
        }
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
}

- (void)postMultipartRequestForImage:(UIImage *)image withFilename:(NSString *)filename toUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure {
    
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n MULTI_PART_SINGLE : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    // [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSData *imageData = UIImagePNGRepresentation(image);
        [formData appendPartWithFileData:imageData name:filename fileName:@"filename.png" mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError * error = nil;
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &error]];
        
        responseDictionary = [self removeNullValues:responseDictionary];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", responseDictionary);
        
        int responseCode = [responseDictionary[@"response_code"] intValue];
        success(responseDictionary, responseCode);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
        
        NSString *serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        NSLog(@"RESPONSE: %@", serverError);
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
    
}

- (void)postMultipartRequestForImages:(NSArray *)images withFilenames:(NSArray *)filenames toUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure {
    
    // Check for Internet Connection
    if (![self isConnected]) {
        NSError *error = [NSError errorWithDomain:urlStr code:-1004 userInfo:@{@"NSErrorFailingURLKey":urlStr}];
        failure(error, [self getErrorMsgForErrorCode:error], nil);
        
        return;
    }
    
    NSLog(@"\n MULTI_PART_MULTIPLE : %@ \n", urlStr);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager] ;
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    // Uncomment this if you have accessToken
    // [manager.requestSerializer setValue:accessToken forHTTPHeaderField:@"X-Auth-Token"];
    
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (UIImage *image in images) {
            int idx = (int)[images indexOfObject:image];
            NSString *filename = [filenames objectAtIndex:idx];
            
            NSData *imageData = UIImagePNGRepresentation(image);
            [formData appendPartWithFileData:imageData name:filename fileName:filename mimeType:@"image/png"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError * error = nil;
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &error]];
        
        responseDictionary = [self removeNullValues:responseDictionary];
        
        NSLog(@"\n +++++++++++++++++++++ SUCCESS_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ SUCCESS_END +++++++++++++++++++++++++++", responseDictionary);
        
        int responseCode = [responseDictionary[@"response_code"] intValue];
        success(responseDictionary, responseCode);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"SERVER ERROR RESPONSE: %@",errorResponse);
        
        NSError *errorDict = nil;
        NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers error:&errorDict]];
        
        NSString *serverError = [responseDictionary objectForKey:SERVER_ERROR_KEY];
        
        NSLog(@"\n +++++++++++++++++++++ FAILURE_START +++++++++++++++++++++++++ \n %@ \n +++++++++++++++++++++ FAILURE_END +++++++++++++++++++++++++", [error description]);
        
        failure(error, [self getErrorMsgForErrorCode:error], serverError);
        
    }];
}


- (NSMutableDictionary *)removeNullValues:(NSMutableDictionary *)responseDictionary {

    if ([[responseDictionary allKeys] containsObject:@"data"]) {
        
        NSMutableArray *newArray = [[NSMutableArray alloc] init];
        if ([[responseDictionary valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in [responseDictionary valueForKey:@"data"]) {
                [newArray addObject:[item dictionaryByReplacingNullsWithStrings]];
            }
            
            [responseDictionary setObject:newArray forKey:@"data"];
        }
        else if ([[responseDictionary valueForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *data = [[responseDictionary valueForKey:@"data"] dictionaryByReplacingNullsWithStrings];
            [responseDictionary setObject:data forKey:@"data"];
        }
        
    }
    
    return responseDictionary;
}

#pragma mark - Error Message Handling

- (NSString *) getErrorMsgForErrorCode:(NSError *) error {
    
    // Set Custom Message for each error code
    NSString *msg;
    
    switch (error.code) {
        case -1004:
            /*
             * NO INTERNET CONNECTION
             */
            msg = [NSString stringWithFormat:@"No Internet Connection."];
            break;
        case -1001:
            /*
             * SERVER MIGHT BE DOWN
             */
            msg = [NSString stringWithFormat:@"%@", [error localizedDescription]];
            break;
        case 200:
            /*
             * INVALID USERNAME / PASSWORD
             */
            msg = [NSString stringWithFormat:@"%@", [error localizedDescription]];
            break;
        default:
            msg = [NSString stringWithFormat:@"%@", [error localizedDescription]];
            break;
    }
    
    return msg;
}

@end
