//
//  LoginVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "MainLoginVC.h"
#import "AppDelegate.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "GlobalMethods.h"

@interface MainLoginVC ()

@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation MainLoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtonAction
- (IBAction)skipLogin:(id)sender
{
    [GlobalMethods saveUserType:YES];
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
}

@end
