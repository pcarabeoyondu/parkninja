//
//  User.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic userId;
@dynamic activationKey;
@dynamic birthday;
@dynamic email;
@dynamic facebookId;
@dynamic fullName;
@dynamic roleId;
@dynamic roles;
@dynamic status;
@dynamic userStatus;
@dynamic userName;
@dynamic mobileNo;
@dynamic displayImage;

@dynamic isGuest;
@dynamic willProceedToPayment;
@dynamic didReceivePushNotif;
@dynamic willNavigateToParking;
@dynamic parkingLat;
@dynamic parkingLong;

@end
