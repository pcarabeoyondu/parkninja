////
//  ParkingLotModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingLotModel.h"

@implementation ParkingLotModel

- (id)initWithParkingLotDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        //bookable
        if (![[dictionary objectForKey:@"availableSlots"] isKindOfClass:[NSNull class]]) {
            self.availableSlots = [[dictionary objectForKey:@"availableSlots"] integerValue];
        } else {
            self.availableSlots = 0;
        }
        if (![[dictionary objectForKey:@"occupiedSlots"] isKindOfClass:[NSNull class]]) {
            self.occupiedSlots = [[dictionary objectForKey:@"occupiedSlots"] integerValue];
        }
        if (![[dictionary objectForKey:@"initialFixedRateAmount"] isKindOfClass:[NSNull class]]) {
            self.initialFixedRate = [[dictionary objectForKey:@"initialFixedRateAmount"] integerValue];
        }
        self.contactDetails = [dictionary objectForKey:@"contactDetails"];
        if (![[dictionary objectForKey:@"merchantPartnerId"] isKindOfClass:[NSNull class]]) {
            self.merchantPartnerId = [[dictionary objectForKey:@"merchantPartnerId"] integerValue];
        }
        if (![[dictionary objectForKey:@"parkingRulesId"] isKindOfClass:[NSNull class]]) {
            self.parkingRulesID = [[dictionary objectForKey:@"parkingRulesId"] integerValue];
        } else {
            self.parkingRulesID = 0;
        }
        
        //nonbookable
        self.motorcycleRate = ![[dictionary objectForKey:@"motorcycleRate"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"motorcycleRate"] : @"";
        self.overnightRate = ![[dictionary objectForKey:@"overnightRate"] isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"overnightRate"] : @"";
        self.weekendRate = ![[dictionary objectForKey:@"weekendRate"] isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"weekendRate"] : @"";
        if (![[dictionary objectForKey:@"withValet"] isKindOfClass:[NSNull class]])
            self.withValet = [[dictionary objectForKey:@"withValet"] boolValue];
        
        
        //both
        if (![[dictionary objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
            self.parkingId = [[dictionary objectForKey:@"id"] integerValue];
        } else {
            self.parkingId = 0;
        }
        
        self.location = ![[dictionary objectForKey:@"location"] isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"location"]: @"";
        self.address = ![[dictionary objectForKey:@"address"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"address"] : @"";
        
        if (![[dictionary objectForKey:@"slots"] isKindOfClass:[NSNull class]])
            self.totalSlots = [[dictionary objectForKey:@"slots"] integerValue];
        else
            self.totalSlots = 0;
        
        if (![[dictionary objectForKey:@"latitude"] isKindOfClass:[NSNull class]]) {
            self.latitude = [[dictionary objectForKey:@"latitude"] floatValue];
        }
        
        if (![[dictionary objectForKey:@"longitude"] isKindOfClass:[NSNull class]]) {
            self.longitude = [[dictionary objectForKey:@"longitude"] floatValue];
        }
        self.operatingHours = ![[dictionary objectForKey:@"operatingHours"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"operatingHours"] : @"";

        
        if ([dictionary objectForKey:@"initialFixedRate"] && [[dictionary objectForKey:@"initialFixedRate"]isKindOfClass:[NSString class]]) {
            self.hourlyRate = [dictionary objectForKey:@"initialFixedRate"];
        }else if ([dictionary objectForKey:@"initialRate"] && [[dictionary objectForKey:@"initialRate"]isKindOfClass:[NSString class]]) {
            self.hourlyRate = [dictionary objectForKey:@"initialRate"];
        }else {
            self.hourlyRate = @"";
        }
        
        self.succeedingRate = ![[dictionary objectForKey:@"succeedingRate"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"succeedingRate"] : @"";
        self.type = ![[dictionary objectForKey:@"type"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"type"] : @"";
        self.imagePath = ![[dictionary objectForKey:@"imageSource"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"imageSource"] : @"";
        self.parkingStatus = ![[dictionary objectForKey:@"parkingStatus"]isKindOfClass:[NSNull class]] ? [dictionary objectForKey:@"parkingStatus"] : @"";
    }
    
    return self;
}

- (id)initWithTransactionDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if (![[dictionary objectForKey:@"parkingLat"] isKindOfClass:[NSNull class]])
            self.latitude = [[dictionary objectForKey:@"parkingLat"] floatValue];
        
        if (![[dictionary objectForKey:@"parkingLng"] isKindOfClass:[NSNull class]])
            self.longitude = [[dictionary objectForKey:@"parkingLng"] floatValue];
    }
    
    return self;
}

- (NSString *)description
{
    NSMutableString *desc = [[NSMutableString alloc]init];
    
    [desc appendString:[NSString stringWithFormat:@"Parking ID:%ld\n",(long)self.parkingId]];
    [desc appendString:[NSString stringWithFormat:@"Location:%@\n",self.location]];
    [desc appendString:[NSString stringWithFormat:@"Address:%@\n",self.address]];
    [desc appendString:[NSString stringWithFormat:@"Total Slots:%ld\n",(long)self.totalSlots]];
    [desc appendString:[NSString stringWithFormat:@"Available Slots:%ld\n",(long)self.availableSlots]];
    [desc appendString:[NSString stringWithFormat:@"Occupied Slots:%ld\n",(long)self.occupiedSlots]];
    [desc appendString:[NSString stringWithFormat:@"Latitude:%f\n",self.latitude]];
    [desc appendString:[NSString stringWithFormat:@"Longitude:%f\n",self.longitude]];
    [desc appendString:[NSString stringWithFormat:@"Operating Hours:%@\n",self.operatingHours]];
    [desc appendString:[NSString stringWithFormat:@"Hourly Rate:%@\n",self.hourlyRate]];
    [desc appendString:[NSString stringWithFormat:@"Initial Fixed Rate:%ld\n",(long)self.initialFixedRate]];
    [desc appendString:[NSString stringWithFormat:@"Succeeding Rate:%@\n",self.succeedingRate]];
    [desc appendString:[NSString stringWithFormat:@"Contact Details:%@\n",self.contactDetails]];
    [desc appendString:[NSString stringWithFormat:@"Type:%@\n",self.type]];
    [desc appendString:[NSString stringWithFormat:@"Merchange Partner ID:%ld\n",(long)self.merchantPartnerId]];
    [desc appendString:[NSString stringWithFormat:@"Bookable:%@\n",(self.isBookable ? @"YES":@"NO")]];
    [desc appendString:[NSString stringWithFormat:@"Image Path:%@\n",self.imagePath]];
    [desc appendString:[NSString stringWithFormat:@"Motorcycle Rate:%@\n",self.motorcycleRate]];
    [desc appendString:[NSString stringWithFormat:@"Overnight Rate:%@\n",self.overnightRate]];
    [desc appendString:[NSString stringWithFormat:@"Weekend Rate:%@\n",self.weekendRate]];
    [desc appendString:[NSString stringWithFormat:@"With Valet?%@\n",(self.withValet ? @"YES":@"NO")]];

    return [NSString stringWithString:desc];
}

@end
