//
//  FailToArriveView.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 4/2/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "FailToArriveView.h"
#import <CoreText/CoreText.h>
#import "GlobalMethods.h"

@implementation FailToArriveView

- (void)initDetails
{
    NSMutableAttributedString *preString = [[NSMutableAttributedString alloc] initWithString:@"If you have any concerns, please contact "];
    [preString addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#6D6D6D"]
                      range:(NSRange){0,[preString length]}];
    
    NSMutableAttributedString *support = [[NSMutableAttributedString alloc] initWithString:@"support@parkninja.ph"];
    [support addAttribute:(NSString *)NSForegroundColorAttributeName
                      value:[GlobalMethods colorFromHexString:@"#3e8bdb"]
                      range:(NSRange){0,[support length]}];
    
    NSMutableAttributedString *orCall = [[NSMutableAttributedString alloc] initWithString:@" or call "];
    [orCall addAttribute:(NSString *)NSForegroundColorAttributeName
                    value:[GlobalMethods colorFromHexString:@"#6D6D6D"]
                    range:(NSRange){0,[orCall length]}];
    
    NSMutableAttributedString *number = [[NSMutableAttributedString alloc] initWithString:@"+63917-543-4594"];
    
    [number addAttribute:(NSString *)NSForegroundColorAttributeName
                        value:[GlobalMethods colorFromHexString:@"#3e8bdb"]
                        range:(NSRange){0,[number length]}];
    
    NSMutableAttributedString *comString = [[NSMutableAttributedString alloc] initWithAttributedString:preString];
    [comString appendAttributedString:support];
    [comString appendAttributedString:orCall];
    [comString appendAttributedString:number];
    
    self.contactDetailsLabel.attributedText = comString;
}

@end
