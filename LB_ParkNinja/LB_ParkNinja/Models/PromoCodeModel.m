//
//  PromoCodeModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/9/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "PromoCodeModel.h"

@implementation PromoCodeModel

- (id)initPromoCodeWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if (![[dictionary objectForKey:@"id"] isKindOfClass:[NSNull class]])
            self.promoCodeId = [[dictionary objectForKey:@"id"] integerValue];
        self.code = [dictionary objectForKey:@"name"];
        
        if (![[dictionary objectForKey:@"discountRateCurrency"] isKindOfClass:[NSNull class]])
            self.discountRateCurrency = [[dictionary objectForKey:@"discountRateCurrency"] integerValue];
        
        if (![[dictionary objectForKey:@"discountRatePercentage"] isKindOfClass:[NSNull class]])
            self.discountRatePercentage = [[dictionary objectForKey:@"discountRatePercentage"] integerValue];
        if (![[dictionary objectForKey:@"isDiscountRateCurrency"] isKindOfClass:[NSNull class]])
            self.isDiscountRateCurrency = [[dictionary objectForKey:@"isDiscountRateCurrency"] boolValue];
        
        if (![[dictionary objectForKey:@"usesPerUser"] isKindOfClass:[NSNull class]])
            self.usesPerUser = [[dictionary objectForKey:@"usesPerUser"] integerValue];
        
        if (![[dictionary objectForKey:@"isFreeParking"] isKindOfClass:[NSNull class]])
            self.isFreeParking = [[dictionary objectForKey:@"isFreeParking"] boolValue];
        
        if ([[dictionary objectForKey:@"available"]isKindOfClass:[NSNumber class]])
            self.available = [[dictionary objectForKey:@"available"] integerValue];
        
        self.status = [dictionary objectForKey:@"status"];
    }
    
    return self;
}

@end
