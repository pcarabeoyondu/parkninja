//
//  ETicketCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/5/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionModel.h"

@interface ETicketCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *referenceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkSiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *qrCodeImage;
@property (weak, nonatomic) IBOutlet UILabel *convenienceFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateReservationLabel;

@property (weak, nonatomic) IBOutlet UILabel *promoCodeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *discounTotalLabel;

@property (weak, nonatomic) IBOutlet UILabel *succeedingHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *succeedingTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;


//static
@property (weak, nonatomic) IBOutlet UILabel *discountPromoLabel;

- (void)initDetailsWithTransactionModel:(TransactionModel *)model;

@end
