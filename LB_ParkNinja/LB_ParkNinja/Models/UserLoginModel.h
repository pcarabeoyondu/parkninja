//
//  UserLoginModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLoginModel : NSObject

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;

@end
