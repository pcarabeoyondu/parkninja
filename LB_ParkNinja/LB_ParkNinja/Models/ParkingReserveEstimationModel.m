//
//  ParkingReserveEstimationModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ParkingReserveEstimationModel.h"

@implementation ParkingReserveEstimationModel

- (id)initParkingEstimationWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];

    if (self)
    {
        self.email = [dictionary objectForKey:@"email"];
        self.ratePrice = [[dictionary objectForKey:@"price"] integerValue];
        self.convenienceFee = [[dictionary objectForKey:@"convenienceFee"] integerValue];
    }
    
    return self;
}

@end
