//
//  ReceiptCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 4/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *parkedAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkedInTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkedOutLabel;
@property (weak, nonatomic) IBOutlet UILabel *tellerNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *flatRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *succeedingRateTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *succeedingRateLabel;

@property (weak, nonatomic) IBOutlet UILabel *convenienceFeeTotalLabel;

@property (weak, nonatomic) IBOutlet UILabel *convenienceFeeLabel
;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;

@property (weak, nonatomic) IBOutlet UILabel *discountTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;



@end
