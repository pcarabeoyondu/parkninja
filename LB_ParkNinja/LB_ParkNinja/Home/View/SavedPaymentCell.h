//
//  SavedPaymentCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/5/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedPaymentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectCreditCardBtn;
@property (weak, nonatomic) IBOutlet UITextField *verificationTextField;
@property (weak, nonatomic) IBOutlet UIButton *addNewCardBtn;


@end
