//
//  UserInformationCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/2/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserVehicleCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UILabel *carName;
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;

@end
