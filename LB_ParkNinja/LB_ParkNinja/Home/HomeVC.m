//
//  HomeSB.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/16/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "HomeVC.h"
#import <MBProgressHUD/MBProgressHUD.h>

#import "ParkListingVC.h"
#import "LocationManager.h"
#import "ParkingLotModel.h"

#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

#import "MapParkingMarker.h"
#import "ParkingLotDetailsVC.h"

#import "MapCallOverView.h"
#import <Reachability/Reachability.h>

#import "ReceiptVC.h"
#import "FailToArriveView.h"
#import "AppDelegate.h"
#import "ParkListingVC.h"

#import "ParkingListManager.h"


@interface HomeVC () {
    NSTimer *tick;
}

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) LocationManager *locationManager;
@property (strong, nonatomic) MapParkingMarker *parkingMarker;
@property (strong, nonatomic) ParkingLotDetailsVC *parkingLotDetails;

@property (strong, nonatomic) NSMutableArray *parkingListArray;
@property (strong, nonatomic) NSMutableArray *searchResultsArray;

@property (strong, nonatomic) NSString *distanceText;
@property (strong, nonatomic) NSString *locationAddress;
@property (strong, nonatomic) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet UIView *disabledView;

@property (nonatomic, assign) BOOL mapDidLoadOnce;
@property (assign, nonatomic) BOOL navigateUser;
@property (assign, nonatomic) BOOL zoomToUserLocation;

@property (strong, nonatomic) IBOutlet MapCallOverView *parkingDetailsView;

@end

@implementation HomeVC
{
    GMSMarker *userMapMarker;
    GMSPolyline *polyline;
    
    FailToArriveView *failedView;
    UIView *cutOffView;
}

+ (HomeVC *)sharedInstance
{
    static dispatch_once_t once;
    static HomeVC *manager;
    
    dispatch_once(&once, ^{
        manager = [[HomeVC alloc] init];
    });
    
    return manager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navController = self.navigationController;
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    [self setUpView];
    
     self.zoomToUserLocation = YES;
    
    [self initLocationManager];
    
    
    [self requestAllParkingList:^(BOOL finished) {
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestNavigationWithNotification:)
                                                 name:@"RequestRoute"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = FALSE;
    
    [super viewWillAppear:animated];
    
    if ([[[GlobalMethods getUser] didReceivePushNotif] boolValue]) {
        [self checkTypeOfReceipt];
    }
    
    [self performSelector:@selector(refreshMap) withObject:nil afterDelay:30];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Push Notif Popups
- (IBAction)showReceiptVC:(id)sender
{
    [cutOffView removeFromSuperview];
    [self showReceiptWithTransactionModel];
    
}

- (IBAction)showCancellationReceiptVC:(id)sender
{
    [failedView removeFromSuperview];
    [self showReceiptWithTransactionModel];
}

#pragma mark - Push Notification
- (void)showPushPopup:(BOOL)failedToArrive
{
    if (failedToArrive)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"FailToArriveView" owner:self options:nil];
        failedView = [subviewArray objectAtIndex:0];
        
        failedView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [failedView initDetails];
        
        [self.navigationController.view addSubview:failedView];
        return;
    }
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingCutOffView" owner:self options:nil];
    cutOffView = [subviewArray objectAtIndex:0];
    
    cutOffView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [failedView initDetails];
    
    [self.navigationController.view addSubview:cutOffView];
}

- (void)checkTypeOfReceipt
{
    NSDictionary *pushDict = (NSDictionary *)[[AppDelegate appDelegate] pushNotifDictionary];
    TransactionModel *transaction = [[TransactionModel alloc] initWithPushNotifDictionary:pushDict];
    
    if ([transaction.eventName isEqualToString:PUSH_CHECK_OUT])
        [self showReceiptWithTransactionModel];
    else if ([transaction.eventName isEqualToString:PUSH_NO_SHOW])
        [self showPushPopup:TRUE];
    else
        [self showPushPopup:FALSE];
    
    
    [GlobalMethods didReceivePushNotif:FALSE];
}

- (void)showReceiptWithTransactionModel
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomeSB" bundle:nil];
    UINavigationController *moreNavController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"receiptNavController"];
    moreNavController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [self.navigationController.topViewController presentViewController:moreNavController animated:YES completion:nil];
}


#pragma mark - Private Methods
- (void) refreshMap {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self requestAllParkingList:^(BOOL finished) {
            [self performSelector:@selector(refreshMap) withObject:nil afterDelay:30];
        }];
    });
}

- (void)setupNavigationItem
{
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_menu"] forState:UIControlStateNormal ];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    
    if (revealViewController)
    {
        [backBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    [backBtn setFrame:CGRectMake(10, 0, 20, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Pick a Parking Spot";
    self.navigationItem.titleView = titleLabelView;
}

- (void)initLocationManager
{
    self.locationManager = [LocationManager sharedInstance];
    [self.locationManager initLocationManager];
    [self.locationManager setDelegate:self];
    
    self.mapDidLoadOnce = NO;
}

- (void)setUpView
{
    [self.mapView setDelegate:self];
}

- (void)setMapParkingPins:(ParkingLotModel *)parkingLot
{
    MapParkingMarker *marker = [[MapParkingMarker alloc] init];
    marker.parkingLot = parkingLot;
    marker.title = parkingLot.location;
    marker.position = CLLocationCoordinate2DMake(parkingLot.latitude, parkingLot.longitude);
    marker.icon = [self setParkingPinsColorWithModel:parkingLot];
    marker.map = self.mapView;
}

- (UIImage *)setParkingPinsColorWithModel:(ParkingLotModel *)model
{
    float takenSlots = 0;
    
    if (model.occupiedSlots <= 0 || model.occupiedSlots > model.totalSlots)
        takenSlots = 0;
    else
        takenSlots = (float)((float)model.occupiedSlots / (float)model.totalSlots) * 100.0;
    
    NSString *pinName = PIN_RED;
    
    
//    if (!model.isBookable)
//        pinName = PIN_UNBOOKABLE;
//    else if (takenSlots < 40)
//        pinName = PIN_GREEN;
//    else if (takenSlots >= 40 && takenSlots < 80)
//        pinName = PIN_YELLOW;
//    else if (takenSlots >= 80 && takenSlots <= 99)
//        pinName = PIN_ORANGE;
//    else
//        pinName = PIN_RED;
    
    if (!model.isBookable)
        pinName = PIN_UNBOOKABLE;
    else if (takenSlots >= 80 && takenSlots <= 99)
        pinName = PIN_ORANGE;
    else if (takenSlots >= 40 && takenSlots < 80)
        pinName = PIN_YELLOW;
    else if (takenSlots < 40)
        pinName = PIN_GREEN;
    else
        pinName = PIN_RED;

    
    NSLog(@"%@", pinName);
    
    return [self image:[UIImage imageNamed:pinName]];
}

- (void)removePolyLine
{
    [polyline setMap:nil];
}

- (void)removeMarkers
{
    [userMapMarker setMap:nil];
    [self.parkingMarker setMap:nil];
}

- (UIImage *)image:(UIImage *)image
{
    CGRect rect = CGRectMake(0, 0, image.size.width / 1.5, image.size.height / 1.5);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [image drawInRect:rect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma  mark - LocationManagerDelegate
- (void)getCurrentUserLocation:(CLLocation *)location
{
    if (!self.mapDidLoadOnce)
    {
        [self.mapView animateToLocation:location.coordinate];
        self.mapDidLoadOnce = TRUE;
        self.currentLocation = location;
        
        if (self.zoomToUserLocation)
            [self.mapView animateToZoom:14];
        
        userMapMarker = [[GMSMarker alloc] init];
        userMapMarker.position = self.currentLocation.coordinate;
        userMapMarker.snippet = @"Current Location";
        userMapMarker.icon = [self image:[UIImage imageNamed:@"pin_user_loc"]];
        userMapMarker.map = self.mapView;
        
        [self.locationManager.locationManager stopUpdatingLocation];
        
        if ([[[GlobalMethods getUser] willNavigateToParking] boolValue])
        {
            [self requestNavigationFromTicketLat:[[[GlobalMethods getUser] parkingLat] floatValue] long:[[[GlobalMethods getUser] parkingLong] floatValue]];
            
            [GlobalMethods willNavigateToParking:FALSE lat:0 long:0];
        }
    }
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SHOW_PARKING_LOT_DETAILS])
    {
        ParkingLotDetailsVC *vc = (ParkingLotDetailsVC *)segue.destinationViewController;
        vc.model = self.parkingMarker.parkingLot;
        
        vc.distance = self.distanceText;
        vc.locationAddress = self.locationAddress;
        vc.location = self.currentLocation;
        return;
    }
    
    ParkListingVC *vc = (ParkListingVC *)segue.destinationViewController;
    vc.location = self.currentLocation;
}

#pragma mark - GMSMapViewDelegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(MapParkingMarker *)marker
{
    mapView.selectedMarker = marker;

    if (marker != userMapMarker)
    {
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(marker.parkingLot.latitude, marker.parkingLot.longitude);
        [self.mapView animateToLocation:centerCoordinate];
    }
    
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    if (marker != userMapMarker && [GlobalMethods isConnected])
    {
        self.parkingMarker = (MapParkingMarker *)marker;
        [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_LOT_DETAILS sender:self];
    }
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(MapParkingMarker *)marker
{
    if (marker == userMapMarker)
        return nil;
    
    self.parkingDetailsView =  [[[NSBundle mainBundle] loadNibNamed:@"MapCallOverView" owner:self options:nil]objectAtIndex:0];
    [self.parkingDetailsView setTranslatesAutoresizingMaskIntoConstraints:NO];

    self.parkingDetailsView.parkingLotNameLabel.text = marker.parkingLot.location;
    self.parkingDetailsView.addressLabel.text = marker.parkingLot.address;
    
    
    float takenSlots = 0;
    
    if (marker.parkingLot.occupiedSlots <= 0 || marker.parkingLot.occupiedSlots > marker.parkingLot.totalSlots)
        takenSlots = 0;
    else
        takenSlots = (float)((float)marker.parkingLot.occupiedSlots / (float)marker.parkingLot.totalSlots) * 100.0;
    
    NSString *hex = @"#cc1e1f";
    
    if (takenSlots < 40)
    {
        hex = @"#2a8446";
    }
    else if (takenSlots >= 40 && takenSlots < 80)
    {
        hex = @"f9cf00";
    }
    else if (takenSlots >= 80 && takenSlots <= 99)
    {
        hex = @"#ea8d06";
    }
    else
    {
        hex = @"#cc1e1f";
    }
    
    if (marker.parkingLot.isBookable)
        self.parkingDetailsView.statusLabel.text = [NSString stringWithFormat:@"%ld available slots", (long)marker.parkingLot.availableSlots];
    else
        self.parkingDetailsView.statusLabel.text = @"Non-bookable";
    
    self.parkingDetailsView.statusLabel.textColor = [GlobalMethods colorFromHexString:hex];
    
    return self.parkingDetailsView;
}

#pragma mark - API Request
- (void)requestAllParkingList:(void(^)(BOOL finished))completion
{
    self.parkingListArray = [NSMutableArray array];
    
    [[ParkingListManager sharedInstance] requestAllParkingList:^(BOOL successful, NSArray *parkingList, NSError *error) {
        if (successful) {
            [self.parkingListArray addObjectsFromArray:parkingList];
            for (ParkingLotModel *model in parkingList) {
                [self setMapParkingPins:model];
            }
            completion (YES);
        }else {
            [GlobalMethods hideProgressHUD];
            completion (YES);
        }
    }];
}

- (BOOL)isSystemVersionEight
{
    return ([[[UIDevice currentDevice] systemVersion] integerValue] >= 8.0);
}

- (BOOL)isLocationServicesAuthorized
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        if ([self isSystemVersionEight])
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Services" message:@"This application needs location services enabled. Please go to settings and allow location access for this app." preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            
            UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                        
                                                               [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
                                                           }];
            
            [alert addAction:cancel];
            [alert addAction:settings];
            
            [self presentViewController:alert animated:YES completion:nil];

            return NO;
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Services" message:@"This application needs location services enabled. Please go to settings and allow location access for this app." preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

- (void)requestNavigationWithNotification:(NSNotification *)notification
{
    if (![self isLocationServicesAuthorized]) {
        NSLog(@"not enabled");
        return;
    }
    
    NSDictionary *dict = notification.userInfo;
    ParkingLotModel *model = [dict objectForKey:@"ParkingModel"];
    
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];

    
    NSString *parkingCoordinate = [NSString stringWithFormat:@"%f,%f", model.latitude, model.longitude];
    
    NSString *baseUrl = [NSString stringWithFormat:URL_REQUEST_GOOGLE_ROUTE, self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, parkingCoordinate];
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:baseUrl withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSDictionary *routes = nil;
        if (((NSArray *) responseDictionary[@"routes"]).count > 0) {
            routes = responseDictionary[@"routes"][0];
        }
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        
        NSDictionary *legs = nil;
        if (((NSArray *) routes[@"legs"]).count > 0) {
            legs = routes[@"legs"][0];
        }
        NSDictionary *distance = legs[@"distance"];
        
        NSString *distanceText = distance[@"text"];
        
        self.parkingMarker.snippet = [NSString stringWithFormat:@"%@ %@", self.parkingMarker.parkingLot.location, distanceText];
        self.parkingMarker.parkingLot.distance = distanceText;
        
        [self removePolyLine];
        
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 4;
        polyline.strokeColor = [UIColor colorWithRed:0.05 green:0.61 blue:0.67 alpha:1.0];
        polyline.map = self.mapView;
        
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake((self.currentLocation.coordinate.latitude + model.latitude)/2, (self.currentLocation.coordinate.longitude + model.longitude)/2);
        
        [self.mapView animateToLocation:centerCoordinate];
        [self.mapView animateToZoom:12];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
         [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestNavigationFromTicketLat:(float)latitude long:(float)longitude
{
    if (![self isLocationServicesAuthorized])
        return;
    
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSString *parkingCoordinate = [NSString stringWithFormat:@"%f,%f", latitude, longitude];
    
    NSString *baseUrl = [NSString stringWithFormat:URL_REQUEST_GOOGLE_ROUTE, self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, parkingCoordinate];
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:baseUrl withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSDictionary *routes = responseDictionary[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        
        NSDictionary *legs = routes[@"legs"][0];
        NSDictionary *distance = legs[@"distance"];
        NSString *distanceText = distance[@"text"];
        
        self.parkingMarker.snippet = [NSString stringWithFormat:@"%@ %@", self.parkingMarker.parkingLot.location, distanceText];
        self.parkingMarker.parkingLot.distance = distanceText;
        
        [self removePolyLine];
        
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 4;
        polyline.strokeColor = [UIColor colorWithRed:0.05 green:0.61 blue:0.67 alpha:1.0];
        polyline.map = self.mapView;
        
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake((self.currentLocation.coordinate.latitude + latitude)/2, (self.currentLocation.coordinate.longitude + longitude)/2);
        
        [self.mapView animateToLocation:centerCoordinate];
        [self.mapView animateToZoom:12];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
         [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - UIButtonAction
- (IBAction)findParking:(id)sender
{
    if (![self isLocationServicesAuthorized])
        return;
    
    if ([GlobalMethods isConnected])
    {
        [self removeMarkers];
        
        userMapMarker = [[GMSMarker alloc] init];
        userMapMarker.position = self.currentLocation.coordinate;
        userMapMarker.snippet = @"Current Location";
        userMapMarker.icon = [self image:[UIImage imageNamed:@"pin_user_loc"]];
        userMapMarker.map = self.mapView;

    [self requestAllParkingList: ^(BOOL finished)
    {
        if (finished)
        {
            UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:[NSString stringWithFormat:@"There are %lu parking spaces available.", (unsigned long)[self.parkingListArray count]]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    
        [self.mapView animateToLocation:self.currentLocation.coordinate];
        [self.mapView animateToZoom:12.5];
    
    }
}

- (IBAction)findMe:(id)sender
{
    if (![self isLocationServicesAuthorized])
        return;
    
    if ([GlobalMethods isConnected])
    {
        [userMapMarker setMap:nil];
        
        self.zoomToUserLocation = NO;
        [self initLocationManager];
    }
}

- (IBAction)showParkListing:(id)sender
{
     [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_LIST sender:self];
}


#pragma mark - SWRevealViewController Delegate Methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionRight)
    {
        self.disabledView.hidden = FALSE;
    }
    else if (position == FrontViewPositionLeft)
    {
        self.disabledView.hidden = TRUE;
    }
}

@end
