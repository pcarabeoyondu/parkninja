//
//  TransactionModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "TransactionModel.h"

@implementation TransactionModel

- (id)initWithTransactionDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if (![[dictionary objectForKey:@"id"] isKindOfClass:[NSNull class]])
            self.transactionId = [[dictionary objectForKey:@"id"] integerValue];
        
        if (![[dictionary objectForKey:@"parkingLat"] isKindOfClass:[NSNull class]])
            self.latitude = [[dictionary objectForKey:@"parkingLat"] floatValue];
        
        if (![[dictionary objectForKey:@"parkingLng"] isKindOfClass:[NSNull class]])
            self.longitude = [[dictionary objectForKey:@"parkingLng"] floatValue];
        
        self.parkingName = [dictionary objectForKey:@"parkingLocation"];
        self.customerName = [dictionary objectForKey:@"customerName"];
        self.plateNum = [dictionary objectForKey:@"plateNo"];
        self.email = [dictionary objectForKey:@"customerEmail"];
        
        self.reservationDate = [dictionary objectForKey:@"reservationDate"];
        
        if (![[dictionary objectForKey:@"flatRate"] isKindOfClass:[NSNull class]])
            self.rate = [[dictionary objectForKey:@"flatRate"] integerValue];
        self.succeedingRate = [dictionary objectForKey:@"succeedingDescription"];
        
        if (![[dictionary objectForKey:@"convenienceFee"] isKindOfClass:[NSNull class]])
            self.convenienceFee = [[dictionary objectForKey:@"convenienceFee"] integerValue];
        self.promoCode = [dictionary objectForKey:@"promoCodeName"];
        
        if (![[dictionary objectForKey:@"totalAmount"] isKindOfClass:[NSNull class]])
            self.totalPrice = [[dictionary objectForKey:@"totalAmount"] integerValue];
        
        if (![[dictionary objectForKey:@"subTotal"] isKindOfClass:[NSNull class]])
            self.subTotalPrice = [[dictionary objectForKey:@"subTotal"] integerValue];
        
        self.transactionCode = [dictionary objectForKey:@"transactionCode"];
        
        if (![[dictionary objectForKey:@"promoCodeDeduction"] isKindOfClass:[NSNull class]])
            self.discountPromo = [[dictionary objectForKey:@"promoCodeDeduction"] integerValue];
        
        if (![[dictionary objectForKey:@"remainingTime"] isKindOfClass:[NSNull class]])
            self.remainingTime = [[dictionary objectForKey:@"remainingTime"] integerValue];
        
        self.isCancellable = [[dictionary objectForKey:@"cancellable"] boolValue];
        self.isCancelled = [[dictionary objectForKey:@"isCancelled"] boolValue];
        self.isActive = [[dictionary objectForKey:@"isActive"] boolValue];
        
    }
    
    return self;
}

- (id)initWithTicketListDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        if (![[dictionary objectForKey:@"id"] isKindOfClass:[NSNull class]])
            self.transactionId = [[dictionary objectForKey:@"id"] integerValue];
        
        if (![[dictionary objectForKey:@"parkingLat"] isKindOfClass:[NSNull class]])
            self.latitude = [[dictionary objectForKey:@"parkingLat"] floatValue];
        
        if (![[dictionary objectForKey:@"parkingLng"] isKindOfClass:[NSNull class]])
            self.longitude = [[dictionary objectForKey:@"parkingLng"] floatValue];
        
        self.parkingName = [dictionary objectForKey:@"parkingLocation"];
        self.customerName = [dictionary objectForKey:@"customerName"];
        self.plateNum = [dictionary objectForKey:@"plateNo"];
        
        self.reservationDate = [dictionary objectForKey:@"reservationDate"];
        
        self.rate = [[dictionary objectForKey:@"flatRate"] integerValue];
        self.succeedingRate = [dictionary objectForKey:@"succeedingDescription"];
        
        if (![[dictionary objectForKey:@"succeeding"] isKindOfClass:[NSNull class]])
            self.succeedingTotal = [[dictionary objectForKey:@"succeeding"] integerValue];
        
        if (![[dictionary objectForKey:@"convenienceFee"] isKindOfClass:[NSNull class]])
            self.convenienceFee = [[dictionary objectForKey:@"convenienceFee"] integerValue];
        self.promoCode = [dictionary objectForKey:@"promoCodeName"];
        
        if (![[dictionary objectForKey:@"totalAmount"] isKindOfClass:[NSNull class]])
            self.totalPrice = [[dictionary objectForKey:@"totalAmount"] integerValue];
        
        if (![[dictionary objectForKey:@"subTotal"] isKindOfClass:[NSNull class]])
            self.subTotalPrice = [[dictionary objectForKey:@"subTotal"] integerValue];
        
        self.transactionCode = [dictionary objectForKey:@"transactionCode"];
        
        if (![[dictionary objectForKey:@"promoCodeDeduction"] isKindOfClass:[NSNull class]])
            self.discountPromo = [[dictionary objectForKey:@"promoCodeDeduction"] integerValue];
        
        if (![[dictionary objectForKey:@"remainingTime"] isKindOfClass:[NSNull class]])
            self.remainingTime = [[dictionary objectForKey:@"remainingTime"] integerValue];
        self.isCancellable = [[dictionary objectForKey:@"cancellable"] boolValue];
        self.isCancelled = [[dictionary objectForKey:@"isCancelled"] boolValue];
        self.isActive = [[dictionary objectForKey:@"isActive"] boolValue];
    }
    
    return self;
}

- (id)initWithPushNotifDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.parkingName = [dictionary objectForKey:@"parkingLocation"];
        self.customerName = [dictionary objectForKey:@"customerName"];
        self.plateNum = [dictionary objectForKey:@"plateNo"];
        
        self.reservationDate = [dictionary objectForKey:@"reservationDate"];
        
        self.checkInTime = [dictionary objectForKey:@"checkIn"];
        self.checkOutTime = [dictionary objectForKey:@"checkOut"];
        
        if (![[dictionary objectForKey:@"flatRate"] isKindOfClass:[NSNull class]])
            self.rate = [[dictionary objectForKey:@"flatRate"] integerValue];
        
        if (![[dictionary objectForKey:@"subTotal"] isKindOfClass:[NSNull class]])
        self.subTotal = [[dictionary objectForKey:@"subTotal"] integerValue];
        
        if (![[dictionary objectForKey:@"subTotal"] isKindOfClass:[NSNull class]])
            self.subTotalPrice = [[dictionary objectForKey:@"subTotal"] integerValue];
        
        if (![[dictionary objectForKey:@"convenienceFee"] isKindOfClass:[NSNull class]])
            self.convenienceFee = [[dictionary objectForKey:@"convenienceFee"] integerValue];
        self.convenienceSucceedingRate = [dictionary objectForKey:@"convenienceDescription"];
        
        self.succeedingRate = [dictionary objectForKey:@"succeedingDescription"];
        
        if (![[dictionary objectForKey:@"succeeding"] isKindOfClass:[NSNull class]])
            self.succeedingTotal = [[dictionary objectForKey:@"succeeding"] integerValue];
    
    
        self.promoCode = [dictionary objectForKey:@"promoCodeName"];
        
        if (![[dictionary objectForKey:@"totalAmount"] isKindOfClass:[NSNull class]])
            self.totalPrice = [[dictionary objectForKey:@"totalAmount"] integerValue];
        
        if (![[dictionary objectForKey:@"promoCodeDeduction"] isKindOfClass:[NSNull class]])
            self.discountPromo = [[dictionary objectForKey:@"promoCodeDeduction"] integerValue];
   
        self.tellerName = [dictionary objectForKey:@"teller"];
        self.email = [dictionary objectForKey:@"email"];
        
        
        self.eventName = [dictionary objectForKey:@"event"];;
    }
    
    return self;
}


@end
