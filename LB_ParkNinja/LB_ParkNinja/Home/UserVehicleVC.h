//
//  UserInformationVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/2/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"

@interface UserVehicleVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *parkingLotModel;
@property (strong, nonatomic) NSString *locationAddress;

@end
