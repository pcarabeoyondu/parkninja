//
//  ParkingListManager.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 7/13/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParkingLotModel.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

@interface ParkingListManager : NSObject

+(ParkingListManager *)sharedInstance;

/**
 * Request both bookable and nonbookable parking list. Will return successful if both or any one request returns successfully.
 */
- (void)requestAllParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion;

/**
 * Request nonbookable parking list from URL_PARK_LIST_NONBOOKABLE
 */
- (void)requestNonBookableParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion;

/**
 * Request bookable parking list from URL_PARK_LIST_BOOKABLE
 */
- (void)requestBookableParkingList:(void(^)(BOOL successful, NSArray*parkingList, NSError *error))completion;
@end
