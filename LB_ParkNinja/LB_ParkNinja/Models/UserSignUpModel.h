//
//  SignUpUserDetailsModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/11/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSignUpModel : NSObject

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *confirmPassword;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *mobileNum;
@property (assign, nonatomic) NSInteger birthday;

@end
