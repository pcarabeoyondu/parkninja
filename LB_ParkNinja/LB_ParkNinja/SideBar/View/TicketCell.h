//
//  TicketCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *referenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkingNameLabel;

@end
