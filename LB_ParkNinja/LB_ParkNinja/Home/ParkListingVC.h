//
//  ParkListingVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"
#import "LocationManager.h"

@interface ParkListingVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *model;
@property (strong, nonatomic) CLLocation* location;

@end
