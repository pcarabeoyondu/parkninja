//
//  PaymentDetailsVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"
#import "ParkingReserveEstimationModel.h"
#import "CarModel.h"
#import "PromoCodeModel.h"

@interface PaymentDetailsVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *parkingLotModel;
@property (strong, nonatomic) ParkingReserveEstimationModel *estimateModel;
@property (strong, nonatomic) CarModel *carModel;
@property (strong, nonatomic) PromoCodeModel *promoModel;

- (void)requestProceedPurchase:(void(^)(BOOL finished))completion;
- (void)requestCardList:(void(^)(BOOL finished))completion;

@end
