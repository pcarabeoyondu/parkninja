//
//  UserInformationVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/2/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "UserVehicleVC.h"
#import "UserVehicleCell.h"
#import "ParkingLotModel.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "ParkingReserveEstimationModel.h"
#import "UserModel.h"
#import "CarModel.h"
#import "User.h"
#import "UserModel.h"
#import "ParkingSummaryVC.h"
#import "GlobalMethods.h"
#import "PromoCodeModel.h"
#import "AddCarCell.h"
#import "TermsView.h"

#import <MBProgressHUD/MBProgressHUD.h>

typedef NS_ENUM(NSInteger, USER_TEXTFIELD_INDEX)
{
    TF_INDEX_CAR_MAKE,
    TF_INDEX_PLATE_NUM,
    TF_INDEX_MODEL,
    TF_INDEX_COLOR
};

@interface UserVehicleVC ()

@property (weak, nonatomic) IBOutlet UITableView *userInfoTableView;
@property (strong, nonatomic) NSMutableArray *parkingList;

@property (strong, nonatomic) ParkingReserveEstimationModel *estimateModel;

@property (assign, nonatomic) NSInteger buttonTag;
@property (strong, nonatomic) NSMutableArray *carListArray;
@property (strong, nonatomic) NSMutableArray *promoListArray;

@property (strong, nonatomic) IBOutlet UILabel *titleHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionHeaderLabel;

@end

@implementation UserVehicleVC
{
    UserModel *userModel;
    User *user;
    CarModel *car;

    BOOL isCarListLoaded;
    
    NSInteger selectedRow;
    
    TermsView *termsView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    if (self.estimateModel == nil)
        self.estimateModel = [[ParkingReserveEstimationModel alloc] init];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    car = [[CarModel alloc] init];
    
    selectedRow = 0;
    
    [self requestCarDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.userInfoTableView addSubview:refreshControl];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self requestCarDetails];
    
    [refreshControl endRefreshing];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal ];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = self.parkingLotModel.location;
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setVCHeader
{
    if ([self.carListArray count] == 0)
    {
        self.titleHeaderLabel.text = @"Add Car";
        self.descriptionHeaderLabel.text = @"Add a car to park";
    }
    else
    {
        self.titleHeaderLabel.text = @"Select a Car";
        self.descriptionHeaderLabel.text = @"Select or add a car to park";
    }
}

#pragma mark - IBAction
- (IBAction)addCar:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add Car"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    NSDictionary *attrDict = @{ NSFontAttributeName:[UIFont fontWithName:@"FSElliotPro-Bold" size:15.0],
                               NSForegroundColorAttributeName:[GlobalMethods colorFromHexString:@"#0C9BAA"]};
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"Add Car" attributes: attrDict];
    [alert setValue:title forKey:@"attributedTitle"];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                   
                                                     NSString *carMake = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     NSString *plateNo = ((UITextField *)[alert.textFields objectAtIndex:1]).text;
                                                     NSString *model = ((UITextField *)[alert.textFields objectAtIndex:2]).text;
                                                     NSString *color = ((UITextField *)[alert.textFields objectAtIndex:3]).text;
                                                     
                                                     if ([GlobalMethods checkInputForWhiteSpaces:plateNo] && [GlobalMethods checkInputForWhiteSpaces:carMake])
                                                     {
                                                         CarModel *carModel = [[CarModel alloc] init];
                                                         carModel.plateNum = plateNo;
                                                         carModel.make = carMake;
                                                         
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:model])
                                                             carModel.model = model;
                                                         else
                                                             carModel.model = @"";
                                                         
                                                         if ([GlobalMethods checkInputForWhiteSpaces:color])
                                                             carModel.color = color;
                                                         else
                                                             carModel.color = @"";
                                                         
                                                         [self requestAddCarWithModel:carModel];
                                                     }
                                                     
                                                     else
                                                     {
                                                         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:MESSAGE_FIELDS_INCOMPLETE];
                                                         [self presentViewController:alert animated:YES completion:nil];
                                                         
                                                     }
                                                 }];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
        textField.placeholder = @"Car Brand";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Plate Number";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Model";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Car Color";
        textField.backgroundColor = [GlobalMethods colorFromHexString:@"#FFFFFF"];
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)showTerms:(id)sender
{
    [self.view endEditing:YES];
    
    if ([GlobalMethods checkInputForWhiteSpaces:car.make] && [GlobalMethods checkInputForWhiteSpaces:car.plateNum])
    {
        if ([self.carListArray count] == 0)
        {
            [self requestAddCarWithModel:car];
        }
        
        else
        {
            [self validateCar];
        }
    } else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:MESSAGE_FIELDS_INCOMPLETE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)cancelTermsView:(id)sender
{
    [termsView removeFromSuperview];
}

- (IBAction)agreeWithTerms:(id)sender
{
    [termsView removeFromSuperview];
    
    [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_SUMMARY sender:self];
}


#pragma mark - API Request
- (void)requestCarDetails
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    self.carListArray = nil;
    
    self.carListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            CarModel *carModel = [[CarModel alloc] initWithCarDictionary:(NSDictionary *)array];
            [self.carListArray addObject:carModel];
        }
        
        isCarListLoaded = TRUE;
        
        [self setVCHeader];
        [self.userInfoTableView reloadData];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestAddCarWithModel:(CarModel *)model
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", model.make, @"make", model.plateNum, @"plateNo", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_ADD withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        [self requestCarDetails];
        
        [GlobalMethods hideProgressHUD];
        
        [self.userInfoTableView reloadData];
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:@"Car has been successfully added."];
        [self presentViewController:alert animated:YES completion:nil];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError)
    {
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)validateCar
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", [NSString stringWithFormat:@"%ld", car.carId], @"id", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_VALIDATE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TermsView" owner:self options:nil];
        termsView = [subviewArray objectAtIndex:0];
        
        termsView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        termsView.termsDescription.text = [NSString stringWithFormat:@"Your reservation will begin upon confirmation and will be held for a maximum of 2 hours. You will be charged a cancellation fee of Php %ld.00.", (long)self.parkingLotModel.initialFixedRate];
        
        [self.view addSubview:termsView];
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];

    }];
    
}

#pragma mark - ParkingInfoCell
- (UserVehicleCell *)setUserInfoWithRow:(NSInteger)row
{
    UserVehicleCell *cell = [self.userInfoTableView dequeueReusableCellWithIdentifier:@"UserVehicleCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"UserVehicleCell" owner:self options:nil];
        cell = (UserVehicleCell *)nibArray[0];
    }
    
    CarModel *model = [self.carListArray objectAtIndex:row];
    cell.carName.text = [NSString stringWithFormat:@"%@ (%@)", model.make, model.plateNum];
    
    if (row == selectedRow)
    {
        car = model;
        cell.bgImageView.image = [UIImage imageNamed:@"bg_text_box_selected"];
        cell.iconImageView.hidden = FALSE;
    }
    
    else
    {
        cell.bgImageView.image = [UIImage imageNamed:@"bg_text_box"];
        cell.iconImageView.hidden = TRUE;
    }
    
    return cell;
}

- (AddCarCell *)setAddCarInfoWithRow:(NSInteger)row
{
    AddCarCell *cell = [self.userInfoTableView dequeueReusableCellWithIdentifier:@"AddCarCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"AddCarCell" owner:self options:nil];
        cell = (AddCarCell *)nibArray[0];
    }
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.carListArray count] == 0)
        return 1;
    
    else if (isCarListLoaded)
        return [self.carListArray count] + 1;
    
    return [self.carListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.carListArray count] == 0)
    {
        return [self setAddCarInfoWithRow:indexPath.row];
    }
    
    else if (indexPath.row == [self.carListArray count])
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddAnotherCarCell" forIndexPath:indexPath];
        
        return cell;
    }
    
    return [self setUserInfoWithRow:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.carListArray count])
        return;
    
    selectedRow = indexPath.row;
    
    [self.userInfoTableView reloadData];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.carListArray count] == 0)
        return 140.0f;
    
    return 60.0f;
}

#pragma mark - UITextFieldDelegate
- (void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _userInfoTableView.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    _userInfoTableView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self setViewMovedUp:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_userInfoTableView scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    });
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    NSString *userInput = textField.text;
    
    switch (textField.tag)
    {
        case TF_INDEX_CAR_MAKE:
        {
            car.make = userInput;
            break;
        }
            
        case TF_INDEX_PLATE_NUM:
        {
            car.plateNum = userInput;
            break;
        }
            
        case TF_INDEX_MODEL:
        {
            car.model = userInput;
            break;
        }
            
        case TF_INDEX_COLOR:
        {
            car.color = userInput;
            break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ParkingSummaryVC *vc = (ParkingSummaryVC *)segue.destinationViewController;
    vc.parkingLotModel = self.parkingLotModel;
    vc.carModel = car;
    vc.locationAddress = self.locationAddress;
}

@end
