//
//  FeedbackVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 04/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "AboutVC.h"
#import "GlobalConstants.h"
#import "AppDelegate.h"
#import "GlobalMethods.h"
#import "WebVC.h"

@interface AboutVC ()
{
    NSString *identifierPath;
}

@end

@implementation AboutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    
    [self setupNavigationItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"About";
    self.navigationItem.titleView = titleLabelView;
}

- (void)openViewWithURL:(NSString *)path
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
}

#pragma mark - IBAction

- (IBAction)dismissViewController:(id)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
}

- (IBAction)showTerms:(id)sender
{
    identifierPath = WEB_TERMS;
    [self performSegueWithIdentifier:WEB_SHOW sender:self];
}

- (IBAction)showFAQ:(id)sender
{
    identifierPath = WEB_FAQ;
    [self performSegueWithIdentifier:WEB_SHOW sender:self];
}

- (IBAction)showPrivacy:(id)sender
{
    identifierPath = WEB_PRIVACY;
    [self performSegueWithIdentifier:WEB_SHOW sender:self];
}

- (IBAction)showTutorial:(id)sender
{
    identifierPath = WEB_TUTORIAL;
    [self performSegueWithIdentifier:WEB_SHOW sender:self];
}

- (IBAction)openFacebook:(id)sender
{
    [self openViewWithURL:URL_FACEBOOK];
}

- (IBAction)openTwitter:(id)sender
{
    [self openViewWithURL:URL_TWITTER];
}

- (IBAction)openInstagram:(id)sender
{
    [self openViewWithURL:URL_INSTAGRAM];
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    WebVC *vc = (WebVC *)segue.destinationViewController;
        
    if ([identifierPath isEqualToString:WEB_TERMS])
        vc.webPath = URL_TERMS;
    else if ([identifierPath isEqualToString:WEB_FAQ])
        vc.webPath = URL_FAQ;
    else if ([identifierPath isEqualToString:WEB_TUTORIAL])
        vc.webPath = URL_TUTORIAL;
    else
        vc.webPath = URL_PRIVACY_POLICY;
}
@end
