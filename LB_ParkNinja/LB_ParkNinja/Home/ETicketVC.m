//
//  ETicketVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ETicketVC.h"
#import "ETicketCell.h"
#import "AppDelegate.h"
#import "GlobalConstants.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import "HttpManagerBlocks.h"
#import "GlobalMethods.h"
#import "User.h"
#import "UserModel.h"

@interface ETicketVC ()

@property (weak, nonatomic) IBOutlet UITableView *eTicketTableView;
@property (strong, nonatomic) UIImage *qrCodeImage;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@end

@implementation ETicketVC
{
    User *user;
    UserModel *userModel;
    
    NSInteger remainingTime;
    NSInteger totalRemainingTime;
    NSTimer *timer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    [self requestQRCode];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.isNavigationBarHidden)
    {
        [self setupNavigationBar];
        [self setupNavigationItem];
        return;
    }
    
    [self.navigationController.navigationBar setHidden:self.isNavigationBarHidden];
}


#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Congratulations";
    self.navigationItem.titleView = titleLabelView;
}

- (void)updateRemainingTime
{
   timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(countDown)
                                           userInfo:nil
                                           repeats:YES];
}

- (void)countDown
{
    remainingTime--;
    
    NSInteger seconds = remainingTime % 60;
    NSInteger minutes = (remainingTime / 60) % 60;
    NSInteger hours = remainingTime / 3600;
    
    if (remainingTime == 0)
    {
        [timer invalidate];
    }
    
    self.remainingTimeLabel.text = [NSString stringWithFormat:@"%ld:%.2ld:%2ld", hours, minutes, seconds];
}

- (void)checkIfCancellable
{
    if (self.transactionModel.isCancellable)
        self.cancelBtn.hidden = FALSE;
    else
        self.cancelBtn.hidden = TRUE;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction
- (IBAction)navigateParkingLocation:(id)sender
{
    if (self.isNavigationBarHidden)
    {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.parkingModel forKey:@"ParkingModel"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RequestRoute" object:nil userInfo:userInfo];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [GlobalMethods willNavigateToParking:TRUE lat:self.parkingModel.latitude long:self.parkingModel.longitude];
        
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [app setRootViewControllerWithSB:SB_MAIN];
    }
}

- (IBAction)cancelReservation:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cancel Reservation" message:@"Are you sure you want to cancel this reservation?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     [self requestCancelParking];
                                                     
                                                 }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [self presentViewController:alert animated:YES completion:nil];

}

#pragma mark - API Request
- (void)requestCancelParking
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[GlobalMethods getUser] email], @"email", [NSNumber numberWithInteger:self.transactionModel.transactionId], @"id", nil];
    
    NSLog(@"USER INFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CANCEL_PARKING withParameters:userInfo withCompletionBlockSuccess: ^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode)
    {
        NSLog(@"%@", responseDictionary);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"This reservation has been successfully cancelled." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         
                                                         if (self.isNavigationBarHidden)
                                                         {
                                                             [self.navigationController popToRootViewControllerAnimated:YES];
                                                         }
                                                         else
                                                         {
                                                             AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                             [app setRootViewControllerWithSB:SB_MAIN];
                                                         }

                                                         
                                                     }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        [GlobalMethods hideProgressHUD];
        
     } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
         
         NSString *errorStr = serverError;
         
         if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
             errorStr = customDescription;
         
         UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
         [self presentViewController:alert animated:YES completion:nil];
         
         [GlobalMethods hideProgressHUD];
         
     }];
}
- (void)requestQRCode
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: self.transactionModel.transactionCode, @"transactionCode", nil];
    
    NSLog(@"USER INFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postQRCodeToUrl:URL_GET_QR_CODE withParameters:userInfo withCompletionBlockSuccess:^(UIImage *response) {
        
        self.qrCodeImage = response;
        
        [GlobalMethods hideProgressHUD];
        
        remainingTime = (self.transactionModel.remainingTime * 60);
        
        if (remainingTime > 0 )
            [self updateRemainingTime];
        
        [self checkIfCancellable];
        
        [self.eTicketTableView reloadData];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
         [GlobalMethods hideProgressHUD];
        
    }];
}

#pragma mark - ParkingInfoCell
- (ETicketCell *)setETicketCellWithRow:(NSInteger)row
{
    ETicketCell *cell = [self.eTicketTableView dequeueReusableCellWithIdentifier:@"ETicketCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ETicketCell" owner:self options:nil];
        cell = (ETicketCell *)nibArray[0];
    }
    
    cell.qrCodeImage.image = self.qrCodeImage;
    
    [cell initDetailsWithTransactionModel:self.transactionModel];

    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setETicketCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 635.0f;
}

@end
