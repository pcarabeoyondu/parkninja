//
//  ParkingLotModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionModel.h"

@interface ParkingLotModel : NSObject

//Exclusive for Bookable
@property (assign, nonatomic) NSInteger     availableSlots;     //key: availableSlots
@property (strong, nonatomic) NSString*     contactDetails;     //key: contactDetails
@property (assign, nonatomic) NSInteger     initialFixedRate;   //key: initialFixedRateAmount
@property (assign, nonatomic) NSInteger     merchantPartnerId;  //key: merchantPartnerId
@property (assign, nonatomic) NSInteger     occupiedSlots;      //key: occupiedSlots
@property (assign, nonatomic) NSInteger     parkingRulesID;     //key: parkingRulesId

//Exclusive for NonBookable
@property (strong, nonatomic) NSString*     motorcycleRate;    //key: motorcycleRate
@property (strong, nonatomic) NSString*     overnightRate;     //key: overnightRate
@property (strong, nonatomic) NSString*     weekendRate;       //key: weekendRate
@property (assign, nonatomic) BOOL          withValet;         //key: withValet

//Both Bookable and NonBookable
@property (strong, nonatomic) NSString*     address;           //key: address
@property (assign, nonatomic) NSInteger     parkingId;         //key: id
@property (strong, nonatomic) NSString*     imagePath;         //key: imageSource
@property (strong, nonatomic) NSString*     hourlyRate;        //key: initialFixedRate(bookable)  ; initialRate(nonbookable)
@property (assign, nonatomic) float         latitude;          //key: latitude
@property (strong, nonatomic) NSString*     location;          //key: location
@property (assign, nonatomic) float         longitude;         //key: longitude
@property (strong, nonatomic) NSString*     operatingHours;    //key: operatingHours
@property (strong, nonatomic) NSString*     parkingStatus;     //key: parkingStatus
@property (assign, nonatomic) NSInteger     totalSlots;        //key: slots
@property (strong, nonatomic) NSString*     succeedingRate;    //key: succeedingRate
@property (strong, nonatomic) NSString*     type;              //key: type

//set from outside
@property (assign, nonatomic) BOOL isBookable;
@property (strong, nonatomic) NSString *distance;

- (id)initWithParkingLotDictionary:(NSDictionary *)dictionary;
- (id)initWithTransactionDictionary:(NSDictionary *)dictionary;

@end

/**
 SAMPLE DICTIONARY FOR NONBOOKABLE PARKING LOT
 
 address = "Toledo, Makati";
 id = 4;
 imageSource = "http://52.74.190.173:8080/parkninja-core/img/p-PaseoSteelCarPark.jpg";
 initialRate = "50.00PHP";
 latitude = "14.5600848";
 location = "Salcedo Park Parking";
 longitude = "121.023268";
 motorcycleRate = "20.00PHP";
 operatingHours = "7:00AM to 10:00AM";
 overnightRate = "100.00PHP";
 parkingStatus = ACTIVE;
 slots = 120;
 succeedingRate = "20.00PHP";
 type = Outdoor;
 weekendRate = "100.00PHP";
 withValet = 1;
 */

/**
 SAMPLE DICTIONARY FOR BOOKABLE PARKING LOT
 
 address = "Lot-2 A2 Bock 13-A Exchange Rd., Antonio Village, Barangay San Antonio, Pasig City";
 availableSlots = 41;
 contactDetails = 123123123123;
 id = 2;
 imageSource = "http://52.74.190.173:8080/parkninja-core/img/p-SalcedoParking.jpg";
 initialFixedRate = "Php 50.00 (first 2 hours) + Php 17.00(Convenience Fee)";
 initialFixedRateAmount = 50;
 latitude = "14.583404";
 location = "PSE Carparks";
 longitude = "121.061850";
 merchantPartnerId = 1;
 occupiedSlots = 59;
 operatingHours = "12:00 AM to 11:00 PM";
 parkingRulesId = 3;
 slots = 100;
 succeedingRate = "Php 20.00 + Php 8.00 (Convenience Fee)";
 type = outdoor;
 
 //empty data that's why we are not getting them yet except for parkingStatus because nonBookable has it.
 earlyBirdFrom = "<null>";
 earlyBirdTo = "<null>";
 hourlyRate = "<null>";
 parkingStatus = "<null>";
 refId = "<null>";
 */
