//
//  PaymentDetailsVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "PaymentDetailsVC.h"
#import "PaymentCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "PaymentVC.h"

#import "User.h"
#import "UserModel.h"
#import "CardModel.h"

#import "ETicketVC.h"
#import "Flurry.h"

@interface PaymentDetailsVC ()

@property (weak, nonatomic) IBOutlet UITableView *paymentDetailsTableView;
@property (strong, nonatomic) NSMutableArray *cardListArray;

@property (strong, nonatomic) CardModel *cardModel;
@property (strong, nonatomic) TransactionModel *transactionModel;

@property (assign, nonatomic) BOOL hasSelectedCardFromList;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;

@end

@implementation PaymentDetailsVC
{
    User *user;
    UserModel *userModel;
    ParkingLotModel *parkingTransactModel;
    
    NSInteger selectedRow;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    self.hasSelectedCardFromList = NO;
    
    [self.continueBtn setImage:[UIImage imageNamed:@"btn_paynow"] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self requestCardList: ^(BOOL finished)
    {
        if ([self.cardListArray count] != 0)
            self.cardModel = (CardModel *)[self.cardListArray objectAtIndex:selectedRow];
     }];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.paymentDetailsTableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [self requestCardList: ^(BOOL finished)
     {
         if ([self.cardListArray count] != 0)
             self.cardModel = (CardModel *)[self.cardListArray objectAtIndex:selectedRow];
     }];
    
    [refreshControl endRefreshing];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Payment Details";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkPromoCodeInput
{
    BOOL isValid = YES;
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.promoModel.code] || ![GlobalMethods checkInputForWhiteSpaces:[NSString stringWithFormat:@"%ld", (long)self.promoModel.promoCodeId]])
        isValid = NO;
    
    return isValid;
}

- (void)checkPaymentToContinue
{
    if ([self.cardListArray count] == 0)
        [self.continueBtn setEnabled:FALSE];
    else
        [self.continueBtn setEnabled:TRUE];
}

#pragma mark - UIButtonAction
- (IBAction)addNewCard:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_SHOW_PAYMENT sender:self];
}

- (IBAction)continuePurchasing:(id)sender
{
    [self requestProceedPurchase:^(BOOL finished)
    {
        [self performSegueWithIdentifier:SEGUE_SHOW_ETICKET sender:self];
    }];
}

#pragma mark - API Request
- (void)requestCardList:(void(^)(BOOL finished))completion
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    self.cardListArray = nil;
    
    self.cardListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: userModel.email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CARD_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            CardModel *cardModel = [[CardModel alloc] initCardModelWithDictionary:(NSDictionary *)array];
            
            if (cardModel.status == 0)
                [self.cardListArray addObject:cardModel];
        }
        
        [self checkPaymentToContinue];
        
        [self.paymentDetailsTableView reloadData];
        
        completion(YES);
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        completion(NO);
        
        [GlobalMethods hideProgressHUD];
    }];
}

- (void)requestProceedPurchase:(void(^)(BOOL finished))completion
{
    MBProgressHUD *HUD = [GlobalMethods showProgressHUD];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    
    NSMutableDictionary *userInfo =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email",[NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", [NSNumber numberWithInteger:self.cardModel.cardId], @"cardId", [NSNumber numberWithInteger:self.carModel.carId], @"carId", nil];
    
    __block NSMutableDictionary *flurryParameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.parkingLotModel.location, @"parkingLocation", [NSNumber numberWithInteger:self.carModel.carId], @"carId", nil];
    
    
    if ([self checkPromoCodeInput])
        [userInfo setObject:[NSNumber numberWithInteger:self.promoModel.promoCodeId] forKey:@"promoCodeId"];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PROCEED_PURCHASE withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode)
     {
         
        self.transactionModel = [[TransactionModel alloc] initWithTransactionDictionary:responseDictionary];
        parkingTransactModel = [[ParkingLotModel alloc] initWithTransactionDictionary:responseDictionary];
         
        [flurryParameters setObject:@"Success" forKey:@"transactionStatus"];
        [Flurry logEvent:@"Parking_Reserve" withParameters:flurryParameters];
        
        completion(YES);
        
        [GlobalMethods hideProgressHUD];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
       
        [flurryParameters setObject:@"Failed" forKey:@"transactionStatus"];
        
        if ([GlobalMethods checkInputForWhiteSpaces:serverError])
            [flurryParameters setObject:serverError forKey:@"transactionMessage"];
        else
            [flurryParameters setObject:customDescription forKey:@"transactionMessage"];
        
        
        [Flurry logEvent:@"Parking_Reserve" withParameters:flurryParameters];
        
        NSString *errorStr = serverError;
        
        if (![GlobalMethods checkInputForWhiteSpaces:errorStr])
            errorStr = customDescription;
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:errorStr];
        [self presentViewController:alert animated:YES completion:nil];
        
        completion(NO);
        [GlobalMethods hideProgressHUD];
    }];
}

#pragma mark - ParkingInfoCell
- (PaymentCell *)setPaymentCellWithRow:(NSInteger)row
{
    PaymentCell *cell = [self.paymentDetailsTableView dequeueReusableCellWithIdentifier:@"PaymentCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"PaymentCell" owner:self options:nil];
        cell = (PaymentCell *)nibArray[0];
    }
    
    CardModel *model = [self.cardListArray objectAtIndex:row];
    cell.cardLabel.text = [NSString stringWithFormat:@"%@ %@", model.type, model.maskedNum];
    
    if (row == selectedRow)
    {
        model = model;
        cell.bgImageView.image = [UIImage imageNamed:@"bg_text_box_selected"];
        cell.iconImageView.hidden = FALSE;
    }
    
    else
    {
        cell.bgImageView.image = [UIImage imageNamed:@"bg_text_box"];
        cell.iconImageView.hidden = TRUE;
    }
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cardListArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.cardListArray count] || [self.cardListArray count] == 0)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddAnotherCarCell" forIndexPath:indexPath];
        
        return cell;
    }
    
    return [self setPaymentCellWithRow:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.cardListArray count])
        return;
    
    selectedRow = indexPath.row;
    
    [self.paymentDetailsTableView reloadData];
    
    self.cardModel = (CardModel *)[self.cardListArray objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SHOW_PAYMENT])
    {
        PaymentVC *vc = (PaymentVC *)segue.destinationViewController;
        vc.isFromPaymentDetails = TRUE;
        return;
    }
    
    ETicketVC *vc = (ETicketVC *)segue.destinationViewController;
    vc.transactionModel = self.transactionModel;
    vc.parkingModel = parkingTransactModel;
    vc.isNavigationBarHidden = YES;
}

@end
