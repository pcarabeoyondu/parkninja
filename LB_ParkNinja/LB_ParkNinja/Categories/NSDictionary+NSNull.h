//
//  NSDictionary+NSNull.h
//  AGCLocator
//
//  Created by Mailyn Sajorda on 4/28/15.
//  Copyright (c) 2015 egg.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSNull)
- (NSDictionary *) dictionaryByReplacingNullsWithStrings;
@end
